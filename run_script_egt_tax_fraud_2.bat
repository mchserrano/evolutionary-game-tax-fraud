
:: DIFFERENT INITIAL POP CONDITIONS

java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_1 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 50 -alpha 0.1
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_2 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 50 -alpha 0.2
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_3 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 50 -alpha 0.3
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_4 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 50 -alpha 0.4
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_5 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 50 -alpha 0.5