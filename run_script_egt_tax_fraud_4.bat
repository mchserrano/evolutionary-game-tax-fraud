
:: REWARDS VALUES FOR ALPHA SA

java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_reward_1 -SA_ALPHA -reward 1
java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_reward_1_25 -SA_ALPHA -reward 1.25
java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_reward_1_5 -SA_ALPHA -reward 1.5
java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_reward_1_75 -SA_ALPHA -reward 1.75
java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_reward_2 -SA_ALPHA -reward 2


:: FINE VALUES FOR ALPHA SA

java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_fine_1 -SA_ALPHA -fine 1
java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_fine_1_25 -SA_ALPHA -fine 1.25
java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_fine_1_5 -SA_ALPHA -fine 1.5
java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_fine_1_75 -SA_ALPHA -fine 1.75
java -jar egt_tax_fraud_0_8_0_2.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_0_8_0_2_fine_2 -SA_ALPHA -fine 2

