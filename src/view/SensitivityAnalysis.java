package view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


import controller.Controller;
import model.Model;
import model.ModelParameters;


/**
 * SA class to run and save results when running the MC model with different parameter configuration
 * from a config base
 * 
 * @author mchica
 * @date 2020/02/03
 * @place Las Palmas GC
 */

public class SensitivityAnalysis {		
	
	// CHANGED TO CONSIDER ABSOLUTE VALUES AND NOT PERCENTAGES!! (BECAUSE OF HAVING DIFFERENCES BETWEEN MACHINES...)

	static int cooperators_min = 0;
	static int cooperators_max = 0;
	static int cooperators_step = 0;
				
	
	// LOGGING
	private static final Logger log = Logger.getLogger( Model.class.getName() );
	
	/**
	 * Static function to run a SA (OAT) for k_C,k_D
	 * 
	 */	
	public static void runSA_kC_kD (ModelParameters _params, String _paramsFile, 
			File fileAllMC, File fileSummaryMCRuns, File fileAllMCLQ, File fileSummaryMCRunsLQ) {

		// create output files in case they exist as we will append the simulation contents
		
		if (fileAllMC.exists() && !fileAllMC.isDirectory()) {
			fileAllMC.delete();
		}
		
		if (fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory()) {
			fileSummaryMCRuns.delete();
		}
		
		if (fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory()) {
			fileAllMCLQ.delete();
		}
		
		if (fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory()) {
			fileSummaryMCRunsLQ.delete();
		}
		
		
		// *********************  Arrays with the k_C, k_T param values */
		
		// create the arrays with the k_C values from min, max and step values
		ArrayList<Integer> values_k_C = new ArrayList<Integer>();
		
		/*int min_k_1 = (int) Math.round(trusters_min * _params.getNrAgents());
		int min_k_2 = (int) Math.round(trustworthies_min * _params.getNrAgents());
		
		int max_k_1 = (int) Math.round(trusters_max * _params.getNrAgents());
		int max_k_2 = (int) Math.round(trustworthies_max * _params.getNrAgents());
		
		int step_k_1 = (int) Math.round(trusters_step * _params.getNrAgents());
		int step_k_2 = (int) Math.round(trustworthies_step * _params.getNrAgents());*/
		
		// CHANGED TO CONSIDER ABSOLUTE VALUES AND NOT PERCENTAGES!! (DIFFERENCES BETWEEN MACHINES)
		int min_k_C = cooperators_min;
		
		int max_k_C = cooperators_max;
		
		int step_k_C = cooperators_step;
		
		// save the values to the array of k1s
		for (int i = min_k_C; i <= max_k_C; i += step_k_C) {
			values_k_C.add(i);
		}
		
		
	    Controller controller = new Controller (_params, _paramsFile);

	    for (int i = 0; i < values_k_C.size(); i++ ) {

	    	
	    	// check if we have more agents of kC than the total number of agents
	    	if ( (values_k_C.get(i)) > _params.getNrAgents()) {
	    		
	    		log.log( Level.FINE, "\n-> OAT Warning: No running for " + values_k_C.get(i) + 
	    				" as there are more k_C than the total number of agents");
	    		
	    		System.out.println("-> OAT Warning: No running for " + values_k_C.get(i) + 
	    				" as there are more k_C than the total number of agents");
	 				   
	    	} else {
	    		
	    		// the number of kCs is correct
	    		
	    		// set the given k values to the params	    	
		    	_params.setk_C(values_k_C.get(i));
		    	_params.setk_D(_params.getNrAgents() - values_k_C.get(i));

		        System.out.println("-> OAT for k_C = " + _params.getk_C() + " k_D = " + _params.getk_D());

				log.log(Level.FINE, "\n****** Running Monte-Carlo simulation for OAT configuration: k_C = " + 
						_params.getk_C() + " k_D = " + _params.getk_D() + " \n");
		
				RunStats stats;

				long time1 = System.currentTimeMillis ();
				
		 		// running the model with the MC simulations
		 		stats = controller.runModel();		

		 		long  time2  = System.currentTimeMillis( );
		 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the OAT simulation");

		 		// calculate the stats for this run and set the name of this experiment
		 		stats.setExpName("k_CValue;" + _params.getk_C() + ";k_DValue;" + _params.getk_D() );		    	
		 		stats.calcAllStats();

				PrintWriter out = new PrintWriter(System.out, true);
				_params.printParameters(out);
				 
		 		// print the stats in the screen 		
		        System.out.println("\n****** Stats of this OAT configuration ******\n");
		 		stats.printSummaryStats (out, false);
		 		stats.printSummaryStatsByAveragingLastQuartile(out, false);

				// print the stats into a file
				System.out.println("\n****** Stats of this OAT configuration also saved into a file ******\n");
				
				PrintWriter printWriter;
		         
		 		try {
		 			
		 			// print all the runs in a file 			
		 			printWriter = new PrintWriter (new FileOutputStream(fileAllMC, true));
		 			
		 			if ( fileAllMC.exists() && !fileAllMC.isDirectory() )
		 		        stats.appendAllStats (printWriter);		 		        
		 		    else
			 			stats.printAllStats (printWriter, false);	
		 	        printWriter.close ();       	
		 	        
		 	        // print the summarized MC runs in a file
		 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRuns, true));
		 	        
		 	        if ( fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory() )
		 	    	    stats.appendSummaryStats (printWriter);		 		        
		 		    else		 		    	
		 		    	stats.printSummaryStats (printWriter, false);	
		 	        printWriter.close ();    
		 	    
		 	        // print all the runs in a file 			
		 			printWriter = new PrintWriter (new FileOutputStream(fileAllMCLQ, true));
		 			
		 			if ( fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory() )
			 			stats.printAllStatsByAveragingLastQuartile(printWriter, true);	      
		 		    else
			 			stats.printAllStatsByAveragingLastQuartile(printWriter, false);	
		 	        printWriter.close ();       	
		 	        
		 	        // print the summarized MC runs in a file
		 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRunsLQ, true));
		 	        
		 	        if ( fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory() )
		 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, true);	
		 		    else		 		    	
		 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);	
		 	        printWriter.close ();    
		 	        
		 		} catch (FileNotFoundException e) {
		 			
		 			// TODO Auto-generated catch block
		 			e.printStackTrace();
		 			
		 		    log.log( Level.SEVERE, e.toString(), e );
		 		} 
	    	}
	    	
	    }	    					
		
	}
	
	/**
	 * Static function to run a SA for the alpha
	 * 
	 */	
	public static void runSA_alpha (ModelParameters _params, String _paramsFile, 
			File fileAllMC, File fileSummaryMCRuns, File fileAllMCLQ, File fileSummaryMCRunsLQ) {

		// create output files in case they exist as we will append the simulation contents
		

		if (fileAllMC.exists() && !fileAllMC.isDirectory()) {
			fileAllMC.delete();
		}
		
		if (fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory()) {
			fileSummaryMCRuns.delete();
		}
		
		if (fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory()) {
			fileAllMCLQ.delete();
		}
		
		if (fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory()) {
			fileSummaryMCRunsLQ.delete();
		}
		
		// *********************  Array with the alpha values 
		
		// create the arrays with the alpha values
		ArrayList<Double> valuesAlpha = new ArrayList<Double>();
		
		// save the values to the array of ratios
		BigDecimal valueParam = new BigDecimal("0.8");
		BigDecimal step = new BigDecimal("0.05");		
		int k = 0;
		while (valueParam.compareTo(new BigDecimal(0)) > -1 ){	
				    	
			valuesAlpha.add(k, valueParam.doubleValue());
			valueParam = valueParam.subtract(step);
			k ++;
		}

	    Controller controller = new Controller (_params, _paramsFile);

	    for (int i = 0; i < valuesAlpha.size(); i++ ) {
		    		
    		// save the current alpha value	    	
	    	_params.setAlpha( valuesAlpha.get(i).floatValue());

	        System.out.println("-> OAT for parameter alpha with a value of " + valuesAlpha.get(i));

			log.log(Level.FINE, "\n****** Running Monte-Carlo simulation for OAT configuration: alpha = " + 
					_params.getAlpha() + " \n");
		
			RunStats stats;

			long time1 = System.currentTimeMillis ();
			
	 		// running the model with the MC simulations
	 		stats = controller.runModel();		

	 		long  time2  = System.currentTimeMillis( );
	 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the alpha SA simulation");

	 		// calculate the stats for this run and set the name of this experiment
	 		stats.setExpName("alphaValue;" + _params.getAlpha());		    	
	 		stats.calcAllStats();

			PrintWriter out = new PrintWriter(System.out, true);
			_params.printParameters(out);
			 
	 		// print the stats in the screen 		
	        System.out.println("\n****** Stats of this OAT configuration ******\n");
	 		stats.printSummaryStats(out, false);
	 		stats.printSummaryStatsByAveragingLastQuartile(out, false);

			// print the stats into a file
			System.out.println("\n****** Stats of this alpha OAT onfiguration also saved into a file ******\n");
			
			PrintWriter printWriter;
	         
	 		try {
	 			
	 			// print all the runs in a file 			
	 			printWriter = new PrintWriter (new FileOutputStream(fileAllMC, true));
	 			
	 			if ( fileAllMC.exists() && !fileAllMC.isDirectory() )
	 		        stats.appendAllStats(printWriter);		 		        
	 		    else
		 			stats.printAllStats(printWriter, false);		 		  
	 		    
	 	        printWriter.close ();       	
	 	        
	 	        // print the summarized MC runs in a file
	 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRuns, true));
	 	        
	 	       if ( fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory() )
	 	    	    stats.appendSummaryStats(printWriter);		 		        
	 		    else		 		    	
	 		    	stats.printSummaryStats(printWriter, false);
	 		    		 				 			
	 	        printWriter.close (); 
	 	        
	 	        // print all the runs in a file 			
	 			printWriter = new PrintWriter (new FileOutputStream(fileAllMCLQ, true));
	 			
	 			if ( fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory() )
		 			stats.printAllStatsByAveragingLastQuartile(printWriter, true);		 		 
	 		    else
		 			stats.printAllStatsByAveragingLastQuartile(printWriter, false);		 		  
	 		    
	 	        printWriter.close ();       	
	 	        
	 	        // print the summarized MC runs in a file
	 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRunsLQ, true));
	 	        
	 	       if ( fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory() )
	 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, true);
	 		    else		 		    	
	 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);
	 		    		 				 			
	 	        printWriter.close ();  
	 	        
	 		} catch (FileNotFoundException e) {
	 			
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 			
	 		    log.log( Level.SEVERE, e.toString(), e );
	 		} 
    	}
    	
    }	
	
	
	/**
	 * Static function to run a SA for the fine value
	 * 
	 */	
	public static void runSA_fine (ModelParameters _params, String _paramsFile, 
			File fileAllMC, File fileSummaryMCRuns, File fileAllMCLQ, File fileSummaryMCRunsLQ) {

		// create output files in case they exist as we will append the simulation contents
		

		if (fileAllMC.exists() && !fileAllMC.isDirectory()) {
			fileAllMC.delete();
		}
		
		if (fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory()) {
			fileSummaryMCRuns.delete();
		}
		
		if (fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory()) {
			fileAllMCLQ.delete();
		}
		
		if (fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory()) {
			fileSummaryMCRunsLQ.delete();
		}
		
		// *********************  Array with the fine values 
		
		// create the arrays with the fine values
		ArrayList<Double> valuesFine = new ArrayList<Double>();
		
		// save the values to the array of values
		BigDecimal valueParam = new BigDecimal("1");
		BigDecimal step = new BigDecimal("0.05");		
		int k = 0;
		while (valueParam.compareTo(new BigDecimal(0)) > -1 ){	
				    	
			valuesFine.add(k, 1. + valueParam.doubleValue());
			valueParam = valueParam.subtract(step);
			k ++;
		}

	    Controller controller = new Controller (_params, _paramsFile);

	    for (int i = 0; i < valuesFine.size(); i++ ) {
		    		
    		// save the current fine value	    	
	    	_params.setFine( valuesFine.get(i).floatValue());

	        System.out.println("-> OAT for parameter fine with a value of " + valuesFine.get(i));

			log.log(Level.FINE, "\n****** Running Monte-Carlo simulation for OAT configuration: fine = " + 
					_params.getFine() + " \n");
		
			RunStats stats;

			long time1 = System.currentTimeMillis ();
			
	 		// running the model with the MC simulations
	 		stats = controller.runModel();		

	 		long  time2  = System.currentTimeMillis( );
	 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the fine SA simulation");

	 		// calculate the stats for this run and set the name of this experiment
	 		stats.setExpName("fineValue;" + _params.getFine());		    	
	 		stats.calcAllStats();

			PrintWriter out = new PrintWriter(System.out, true);
			_params.printParameters(out);
			 
	 		// print the stats in the screen 		
	        System.out.println("\n****** Stats of this OAT configuration ******\n");
	 		stats.printSummaryStats(out, false);
	 		stats.printSummaryStatsByAveragingLastQuartile(out, false);

			// print the stats into a file
			System.out.println("\n****** Stats of this fine OAT onfiguration also saved into a file ******\n");
			
			PrintWriter printWriter;
	         
	 		try {
	 			
	 			// print all the runs in a file 			
	 			printWriter = new PrintWriter (new FileOutputStream(fileAllMC, true));
	 			
	 			if ( fileAllMC.exists() && !fileAllMC.isDirectory() )
	 		        stats.appendAllStats(printWriter);		 		        
	 		    else
		 			stats.printAllStats(printWriter, false);		 		  
	 		    
	 	        printWriter.close ();       	
	 	        
	 	        // print the summarized MC runs in a file
	 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRuns, true));
	 	        
	 	       if ( fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory() )
	 	    	    stats.appendSummaryStats(printWriter);		 		        
	 		    else		 		    	
	 		    	stats.printSummaryStats(printWriter, false);
	 		    		 				 			
	 	        printWriter.close (); 
	 	        
	 	        // print all the runs in a file 			
	 			printWriter = new PrintWriter (new FileOutputStream(fileAllMCLQ, true));
	 			
	 			if ( fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory() )
		 			stats.printAllStatsByAveragingLastQuartile(printWriter, true);		 		 
	 		    else
		 			stats.printAllStatsByAveragingLastQuartile(printWriter, false);		 		  
	 		    
	 	        printWriter.close ();       	
	 	        
	 	        // print the summarized MC runs in a file
	 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRunsLQ, true));
	 	        
	 	       if ( fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory() )
	 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, true);
	 		    else		 		    	
	 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);
	 		    		 				 			
	 	        printWriter.close ();  
	 	        
	 		} catch (FileNotFoundException e) {
	 			
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 			
	 		    log.log( Level.SEVERE, e.toString(), e );
	 		} 
    	}
    	
    }	
	
	
	/**
	 * Static function to run a SA for the reward value
	 * 
	 */	
	public static void runSA_reward (ModelParameters _params, String _paramsFile, 
			File fileAllMC, File fileSummaryMCRuns, File fileAllMCLQ, File fileSummaryMCRunsLQ) {

		// create output files in case they exist as we will append the simulation contents
		
		if (fileAllMC.exists() && !fileAllMC.isDirectory()) {
			fileAllMC.delete();
		}
		
		if (fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory()) {
			fileSummaryMCRuns.delete();
		}
		
		if (fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory()) {
			fileAllMCLQ.delete();
		}
		
		if (fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory()) {
			fileSummaryMCRunsLQ.delete();
		}
		
		// *********************  Array with the reward values 
		
		// create the arrays with the reward values
		ArrayList<Double> valuesReward = new ArrayList<Double>();
		
		// save the values to the array of values
		BigDecimal valueParam = new BigDecimal("1");
		BigDecimal step = new BigDecimal("0.05");		
		int k = 0;
		while (valueParam.compareTo(new BigDecimal(0)) > -1 ){	
				    	
			valuesReward.add(k, 1. + valueParam.doubleValue());
			valueParam = valueParam.subtract(step);
			k ++;
		}

	    Controller controller = new Controller (_params, _paramsFile);

	    for (int i = 0; i < valuesReward.size(); i++ ) {
		    		
    		// save the current reward value	    	
	    	_params.setReputationalReward( valuesReward.get(i).floatValue());

	        System.out.println("-> OAT for parameter reputational reward with a value of " + valuesReward.get(i));

			log.log(Level.FINE, "\n****** Running Monte-Carlo simulation for OAT configuration: R = " + 
					_params.getReputationalReward() + " \n");
		
			RunStats stats;

			long time1 = System.currentTimeMillis ();
			
	 		// running the model with the MC simulations
	 		stats = controller.runModel();		

	 		long  time2  = System.currentTimeMillis( );
	 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the fine SA simulation");

	 		// calculate the stats for this run and set the name of this experiment
	 		stats.setExpName("rewardValue;" + _params.getReputationalReward());		    	
	 		stats.calcAllStats();

			PrintWriter out = new PrintWriter(System.out, true);
			_params.printParameters(out);
			 
	 		// print the stats in the screen 		
	        System.out.println("\n****** Stats of this OAT configuration ******\n");
	 		stats.printSummaryStats(out, false);
	 		stats.printSummaryStatsByAveragingLastQuartile(out, false);

			// print the stats into a file
			System.out.println("\n****** Stats of this fine OAT onfiguration also saved into a file ******\n");
			
			PrintWriter printWriter;
	         
	 		try {
	 			
	 			// print all the runs in a file 			
	 			printWriter = new PrintWriter (new FileOutputStream(fileAllMC, true));
	 			
	 			if ( fileAllMC.exists() && !fileAllMC.isDirectory() )
	 		        stats.appendAllStats(printWriter);		 		        
	 		    else
		 			stats.printAllStats(printWriter, false);		 		  
	 		    
	 	        printWriter.close ();       	
	 	        
	 	        // print the summarized MC runs in a file
	 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRuns, true));
	 	        
	 	       if ( fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory() )
	 	    	    stats.appendSummaryStats(printWriter);		 		        
	 		    else		 		    	
	 		    	stats.printSummaryStats(printWriter, false);
	 		    		 				 			
	 	        printWriter.close (); 
	 	        
	 	        // print all the runs in a file 			
	 			printWriter = new PrintWriter (new FileOutputStream(fileAllMCLQ, true));
	 			
	 			if ( fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory() )
		 			stats.printAllStatsByAveragingLastQuartile(printWriter, true);		 		 
	 		    else
		 			stats.printAllStatsByAveragingLastQuartile(printWriter, false);		 		  
	 		    
	 	        printWriter.close ();       	
	 	        
	 	        // print the summarized MC runs in a file
	 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRunsLQ, true));
	 	        
	 	       if ( fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory() )
	 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, true);
	 		    else		 		    	
	 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);
	 		    		 				 			
	 	        printWriter.close ();  
	 	        
	 		} catch (FileNotFoundException e) {
	 			
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 			
	 		    log.log( Level.SEVERE, e.toString(), e );
	 		} 
    	}
    	
    }	
	
	
	/**
	 * Static function to run a SA (OAT) for alpha, probDHigh
	 * 
	 */	
	public static void runSA_alpha_probDHigh (ModelParameters _params, String _paramsFile, 
			File fileAllMC, File fileSummaryMCRuns, File fileAllMCLQ, File fileSummaryMCRunsLQ) {

		// create output files in case they exist as we will append the simulation contents
		
		if (fileAllMC.exists() && !fileAllMC.isDirectory()) {
			fileAllMC.delete();
		}
		
		if (fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory()) {
			fileSummaryMCRuns.delete();
		}
		
		if (fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory()) {
			fileAllMCLQ.delete();
		}
		
		if (fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory()) {
			fileSummaryMCRunsLQ.delete();
		}
		
				
		// *********************  Array with the alpha values 
		
		// create the arrays with the alpha values
		ArrayList<Float> valuesAlpha = new ArrayList<Float>();
		
		// save the values to the array of ratios
		BigDecimal valueParam = new BigDecimal("0.4");
		BigDecimal step = new BigDecimal("0.05");		
		int k = 0;
		while (valueParam.compareTo(new BigDecimal(0)) > -1 ){					    	
			valuesAlpha.add(k, valueParam.floatValue());
			valueParam = valueParam.subtract(step);
			k ++;
		}
		
		// *********************  Array with the probDHigh values 
		
		// create the arrays with the probDHigh values
		ArrayList<Float> valuesProbDHigh = new ArrayList<Float>();
		
		// save the values to the array of ratios
		valueParam = new BigDecimal("0.3");
		step = new BigDecimal("0.05");		
		k = 0;
		while (valueParam.compareTo(new BigDecimal(0)) > -1 ){					    	
			valuesProbDHigh.add(k, valueParam.floatValue());
			valueParam = valueParam.subtract(step);
			k ++;
		}
				
				
	    Controller controller = new Controller (_params, _paramsFile);

	    for (int i = 0; i < valuesAlpha.size(); i++ ) {

		    for (int j = 0; j < valuesProbDHigh.size(); j++ ) {
	    		    		
	    		// set the values to the params	for the SA	
		    	_params.setAlpha(valuesAlpha.get(i));
		    	_params.setProbDHigh(valuesProbDHigh.get(j));

		        System.out.println("-> OAT for alpha = " + _params.getAlpha() + " probDHigh = " + _params.getProbDHigh());

				log.log(Level.FINE, "\n****** Running Monte-Carlo simulation for OAT configuration: alpha = " + 
						_params.getAlpha() + " probDHigh = " + _params.getProbDHigh() + " \n");
		
				RunStats stats;

				long time1 = System.currentTimeMillis ();
				
		 		// running the model with the MC simulations
		 		stats = controller.runModel();		

		 		long  time2  = System.currentTimeMillis( );
		 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the OAT simulation");

		 		// calculate the stats for this run and set the name of this experiment
		 		stats.setExpName("alphaValue;" + _params.getAlpha() + ";probDHighValue;" + _params.getProbDHigh() );		    	
		 		stats.calcAllStats();

				PrintWriter out = new PrintWriter(System.out, true);
				_params.printParameters(out);
				 
		 		// print the stats in the screen 		
		        System.out.println("\n****** Stats of this OAT configuration ******\n");
		 		stats.printSummaryStats (out, false);
		 		stats.printSummaryStatsByAveragingLastQuartile(out, false);

				// print the stats into a file
				System.out.println("\n****** Stats of this OAT configuration also saved into a file ******\n");
				
				PrintWriter printWriter;
		         
		 		try {
		 			
		 			// print all the runs in a file 			
		 			printWriter = new PrintWriter (new FileOutputStream(fileAllMC, true));
		 			
		 			if ( fileAllMC.exists() && !fileAllMC.isDirectory() )
		 		        stats.appendAllStats (printWriter);		 		        
		 		    else
			 			stats.printAllStats (printWriter, false);	
		 	        printWriter.close ();       	
		 	        
		 	        // print the summarized MC runs in a file
		 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRuns, true));
		 	        
		 	        if ( fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory() )
		 	    	    stats.appendSummaryStats (printWriter);		 		        
		 		    else		 		    	
		 		    	stats.printSummaryStats (printWriter, false);	
		 	        printWriter.close ();    
		 	    
		 	        // print all the runs in a file 			
		 			printWriter = new PrintWriter (new FileOutputStream(fileAllMCLQ, true));
		 			
		 			if ( fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory() )
			 			stats.printAllStatsByAveragingLastQuartile(printWriter, true);	      
		 		    else
			 			stats.printAllStatsByAveragingLastQuartile(printWriter, false);	
		 	        printWriter.close ();       	
		 	        
		 	        // print the summarized MC runs in a file
		 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRunsLQ, true));
		 	        
		 	        if ( fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory() )
		 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, true);	
		 		    else		 		    	
		 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);	
		 	        printWriter.close ();    
		 	        
		 		} catch (FileNotFoundException e) {
		 			
		 			// TODO Auto-generated catch block
		 			e.printStackTrace();
		 			
		 		    log.log( Level.SEVERE, e.toString(), e );
		 		} 
	    	
		    }
	    }	    					
		
	}
	
	/**
	 * Static function to run a SA (OAT) for alpha, inspectionCost
	 * 
	 */	
	public static void runSA_alpha_inspectionCost (ModelParameters _params, String _paramsFile, 
			File fileAllMC, File fileSummaryMCRuns, File fileAllMCLQ, File fileSummaryMCRunsLQ) {

		// create output files in case they exist as we will append the simulation contents
		
		if (fileAllMC.exists() && !fileAllMC.isDirectory()) {
			fileAllMC.delete();
		}
		
		if (fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory()) {
			fileSummaryMCRuns.delete();
		}
		
		if (fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory()) {
			fileAllMCLQ.delete();
		}
		
		if (fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory()) {
			fileSummaryMCRunsLQ.delete();
		}
		
				
		// *********************  Array with the alpha values 
		
		// create the arrays with the alpha values
		ArrayList<Float> valuesAlpha = new ArrayList<Float>();
		
		// save the values to the array of ratios
		BigDecimal valueParam = new BigDecimal("0.8");
		BigDecimal step = new BigDecimal("0.05");		
		int k = 0;
		while (valueParam.compareTo(new BigDecimal(0)) > -1 ){					    	
			valuesAlpha.add(k, valueParam.floatValue());
			valueParam = valueParam.subtract(step);
			k ++;
		}
		
		// *********************  Array with the inspectionCost values 
		
		// create the arrays with the inspectionCost values
		ArrayList<Float> valuesInspectionCost = new ArrayList<Float>();
		
		// save the values to the array of ratios
		valueParam = new BigDecimal("2");
		step = new BigDecimal("0.1");		
		k = 0;
		while (valueParam.compareTo(new BigDecimal(0)) > -1 ){					    	
			valuesInspectionCost.add(k, valueParam.floatValue());
			valueParam = valueParam.subtract(step);
			k ++;
		}
				
				
	    Controller controller = new Controller (_params, _paramsFile);

	    for (int i = 0; i < valuesAlpha.size(); i++ ) {

		    for (int j = 0; j < valuesInspectionCost.size(); j++ ) {
	    		    		
	    		// set the values to the params	for the SA	
		    	_params.setAlpha(valuesAlpha.get(i));
		    	_params.setInspectionCost(valuesInspectionCost.get(j));

		        System.out.println("-> OAT for alpha = " + _params.getAlpha() + " inspectionCost = " + _params.getInspectionCost());

				log.log(Level.FINE, "\n****** Running Monte-Carlo simulation for OAT configuration: alpha = " + 
						_params.getAlpha() + " inspectionCost = " + _params.getInspectionCost() + " \n");
		
				RunStats stats;

				long time1 = System.currentTimeMillis ();
				
		 		// running the model with the MC simulations
		 		stats = controller.runModel();		

		 		long  time2  = System.currentTimeMillis( );
		 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the OAT simulation");

		 		// calculate the stats for this run and set the name of this experiment
		 		stats.setExpName("alphaValue;" + _params.getAlpha() + ";inspectionCost;" + _params.getInspectionCost() );		    	
		 		stats.calcAllStats();

				PrintWriter out = new PrintWriter(System.out, true);
				_params.printParameters(out);
				 
		 		// print the stats in the screen 		
		        System.out.println("\n****** Stats of this OAT configuration ******\n");
		 		stats.printSummaryStats (out, false);
		 		stats.printSummaryStatsByAveragingLastQuartile(out, false);

				// print the stats into a file
				System.out.println("\n****** Stats of this OAT configuration also saved into a file ******\n");
				
				PrintWriter printWriter;
		         
		 		try {
		 			
		 			// print all the runs in a file 			
		 			printWriter = new PrintWriter (new FileOutputStream(fileAllMC, true));
		 			
		 			if ( fileAllMC.exists() && !fileAllMC.isDirectory() )
		 		        stats.appendAllStats (printWriter);		 		        
		 		    else
			 			stats.printAllStats (printWriter, false);	
		 	        printWriter.close ();       	
		 	        
		 	        // print the summarized MC runs in a file
		 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRuns, true));
		 	        
		 	        if ( fileSummaryMCRuns.exists() && !fileSummaryMCRuns.isDirectory() )
		 	    	    stats.appendSummaryStats (printWriter);		 		        
		 		    else		 		    	
		 		    	stats.printSummaryStats (printWriter, false);	
		 	        printWriter.close ();    
		 	    
		 	        // print all the runs in a file 			
		 			printWriter = new PrintWriter (new FileOutputStream(fileAllMCLQ, true));
		 			
		 			if ( fileAllMCLQ.exists() && !fileAllMCLQ.isDirectory() )
			 			stats.printAllStatsByAveragingLastQuartile(printWriter, true);	      
		 		    else
			 			stats.printAllStatsByAveragingLastQuartile(printWriter, false);	
		 	        printWriter.close ();       	
		 	        
		 	        // print the summarized MC runs in a file
		 			printWriter = new PrintWriter (new FileOutputStream(fileSummaryMCRunsLQ, true));
		 	        
		 	        if ( fileSummaryMCRunsLQ.exists() && !fileSummaryMCRunsLQ.isDirectory() )
		 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, true);	
		 		    else		 		    	
		 		    	stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);	
		 	        printWriter.close ();    
		 	        
		 		} catch (FileNotFoundException e) {
		 			
		 			// TODO Auto-generated catch block
		 			e.printStackTrace();
		 			
		 		    log.log( Level.SEVERE, e.toString(), e );
		 		} 
	    	
		    }
	    }	    					
		
	}
		
}
