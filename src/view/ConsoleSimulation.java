package view;

import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import controller.Controller;
import model.Model;
import model.ModelParameters;
import util.Functions;

//----------------------------- MAIN FUNCTION -----------------------------//

/**
 * Main function for EGT for tax fraud
 * 
 * @author mchica
 * @date 2020/02/03
 * @place Las Palmas GC
 */

public class ConsoleSimulation {		

	// constants to get different options for simple or SA runs
	public final static int NO_SA = 0;
	public final static int SA_INITIAL_POPULATION = 1;
	public final static int SA_ALPHA = 2;
	public final static int SA_ALPHA_PROBDHIGH = 3;
	public final static int SA_ALPHA_INSPECTIONCOST = 6;
	public final static int SA_REWARD = 4;
	public final static int SA_FINE = 5;
	
	
	// LOGGING
	private static final Logger log = Logger.getLogger( Model.class.getName() );
	
	/**
	 * Create an options class to store all the arguments of the command-line call of the program
	 * 
	 * @param options the class containing the options, when returned. It has to be created before calling
	 */
	private static void createArguments (Options options) {
				
		options.addOption("paramsFile", true, "Pathfile with the parameters file");
		options.getOption("paramsFile").setRequired(true);
		
		options.addOption("SNFile", true, "File with the SN to run");
		
		options.addOption("outputFile", true, "File to store all the information about the simulation");
		options.getOption("outputFile").setRequired(true);

		options.addOption("percentageCooperators", true, "% of agents to be cooperators at the beginning of the simulation");
		options.addOption("probDHigh", true, "probability for a SN edge to have high declaration value");
		
		options.addOption("alpha", true, "alpha value");
		options.addOption("inspectionCost", true, "inspectionCost value");
		options.addOption("fine", true, "fine value");
		options.addOption("reward", true, "reward value");

		options.addOption("mutProb", true, "Probability for mutating the strategy");
		options.addOption("maxSteps", true, "Max number of steps of the simulation");

		options.addOption("MC", true, "Number of MC simulations");
		options.addOption("seed", true, "Seed for running the MC simulations");
				
		// parameters for SA
		options.addOption("SA", false, "If running a SA over the initial population parameters");	
		options.addOption("SA_ALPHA", false, "If running a SA over the alpha parameter");	
		options.addOption("SA_FINE", false, "If running a SA over the fine parameter");	
		options.addOption("SA_REWARD", false, "If running a SA over the reputational reward parameter");	
		options.addOption("SA_ALPHA_PROBDHIGH", false, "If running a SA over the alpha and probDHigh parameter");	
		options.addOption("SA_ALPHA_INSPECTIONCOST", false, "If running a SA over the alpha and inspectionCost parameter");	
				
		options.addOption("SA_cooperators_min", true, "Minimum value for parameter % of cooperators (OAT SA)");
		options.addOption("SA_cooperators_max", true, "Maximum value for parameter % of cooperators (OAT SA)");
		options.addOption("SA_cooperators_step", true, "Step value forparameter % of cooperators (OAT SA)");

		// to log additional information for studying dynamics of agents
		options.addOption("output_whole_evolution", false, "To save in files additional information of the whole system evolution");	
					
		// to show help
		options.addOption("help", false, "Show help information");	
					
		
	}
	
	/**
	 * MAIN CONSOLE-BASED FUNCTION TO RUN A SIMPLE RUN OR A SENSITIVITY ANALYSIS OF THE MODEL PARAMETERS
	 * @param args
	 */
	public static void main (String[] args) {
		
		int SA = NO_SA;
		   	
    	
		String paramsFile = "";
		String outputFile = "";

		ModelParameters params = null;
		
		// parsing the arguments
		Options options = new Options();
		
		createArguments (options);		

		// create the parser
	    CommandLineParser parser = new DefaultParser();
	    
	    try {
	    	
	        // parse the command line arguments for the given options
	        CommandLine line = parser.parse( options, args );

			// get parameters
			params = new ModelParameters();	
			
	        // retrieve the arguments
	        		    
		    if( line.hasOption( "paramsFile" ) )		    
		    	paramsFile = line.getOptionValue("paramsFile");
		    else 		    	
		    	System.err.println( "A parameters file is needed");

		    if( line.hasOption( "outputFile" ) ) 			    
		    	outputFile = line.getOptionValue("outputFile");

		    // read parameters from file
			params.readParameters(paramsFile);

			// set the outputfile
			params.setOutputFile(outputFile);
						
			// once parameters from file are loaded, we modify those read by arguments of command line		
			
		    // load the parameters file and later, override them if there are console arguments for these parameters
		    if( line.hasOption( "percentageCooperators" ) ) 			    
		    	params.setPercentageCooperators(Float.parseFloat(line.getOptionValue("percentageCooperators")));
		    
		    if( line.hasOption( "probDHigh" ) ) 			    
		    	params.setProbDHigh(Float.parseFloat(line.getOptionValue("probDHigh")));
			
		    // to update kC and kD from percentages
		    params.updateKsFromPercentages();


		    // save file for the SN
		    if( line.hasOption( "SNFile" ) )		    
		    	params.setNetworkFilesPattern(line.getOptionValue("SNFile"));		    	
			  
		    // MC
		    if( line.hasOption( "MC" ) ) 			    
		    	params.setRunsMC(Integer.parseInt(line.getOptionValue("MC")));
		  
		    // seed
		    if( line.hasOption( "seed" ) ) 			    
		    	params.setSeed(Long.parseLong(line.getOptionValue("seed")));

		    // maxSteps
		    if( line.hasOption( "maxSteps" ) ) 			    
		    	params.setMaxSteps(Integer.parseInt(line.getOptionValue("maxSteps")));
		    	    		
		    // alpha
		    if( line.hasOption( "alpha" ) ) 			    
		    	params.setAlpha(Float.parseFloat(line.getOptionValue("alpha")));
		    	    		
		    // inspection cost
		    if( line.hasOption( "inspectionCost" ) ) 			    
		    	params.setInspectionCost(Float.parseFloat(line.getOptionValue("inspectionCost")));

		    // fine
		    if( line.hasOption( "fine" ) ) 			    
		    	params.setFine(Float.parseFloat(line.getOptionValue("fine")));
		    
		    // reward
		    if( line.hasOption( "reward" ) ) 			    
		    	params.setReputationalReward(Float.parseFloat(line.getOptionValue("reward")));
		    
		    // prob D High
		    if( line.hasOption( "probDHigh" ) ) 			    
		    	params.setProbDHigh(Float.parseFloat(line.getOptionValue("probDHigh")));
		    
		    // prob mut
		    if( line.hasOption( "mutProb" ) ) 			    
		    	params.setMutProb(Float.parseFloat(line.getOptionValue("mutProb")));
		    
		    
		    // % cooperators
		    if( line.hasOption( "percentageCooperators" ) ) 			    
		    	params.setPercentageCooperators(Float.parseFloat(line.getOptionValue("percentageCooperators")));

		    
			setDLDHAndProbs(params);
			
		
		    // help information
		    if( line.hasOption("help") ) {
			    	
			    // automatically generate the help statement
			    HelpFormatter formatter = new HelpFormatter();
			    formatter.printHelp( "TrustDynamics. Last update April 2019. Manuel Chica", options);			   	
			}	
		  		    		    
		    // check if we have to store additional information
		    
		    if(line.hasOption("output_whole_evolution") ) {			    	
		    	ModelParameters.OUTPUT_WHOLE_EVOLUTION  = true;
			}	   
	
		    				
		    // if a SA is running
		    if( line.hasOption( "SA" ) ) {
		    	
		    	SA = SA_INITIAL_POPULATION;
		    	
		    	// we have to get the rest of the parameters of the SA running (they were changed to integer instead of ratios because of 
		    	// differences between machines when calculating the exact number of agents)
		    	
		    	if (line.hasOption( "SA_cooperators_min" ))
		    		SensitivityAnalysis.cooperators_min = Integer.parseInt(line.getOptionValue("SA_cooperators_min"));
		    	if (line.hasOption( "SA_cooperators_max" ))
		    		SensitivityAnalysis.cooperators_max = Integer.parseInt(line.getOptionValue("SA_cooperators_max"));
		    	if (line.hasOption( "SA_cooperators_step" ))
		    		SensitivityAnalysis.cooperators_step = Integer.parseInt(line.getOptionValue("SA_cooperators_step"));
		    	
		    } else if( line.hasOption( "SA_ALPHA" ) ) {
		    	
		    	// we have to run a SA on the alpha param
		    	
		    	SA = SA_ALPHA;
		    	
		    } else if( line.hasOption( "SA_REWARD" ) ) {
		    	
		    	// we have to run a SA on the R param
		    	
		    	SA = SA_REWARD;
		    	
		    } else if( line.hasOption( "SA_FINE" ) ) {
		    	
		    	// we have to run a SA on the fine param
		    	
		    	SA = SA_FINE;
		    	
		    } else if( line.hasOption( "SA_ALPHA_PROBDHIGH" ) ) {
		    	
		    	// we have to run a SA on the alpha/prob dhigh param
		    	
		    	SA = SA_ALPHA_PROBDHIGH;
		    	
		    } else if( line.hasOption( "SA_ALPHA_INSPECTIONCOST" ) ) {
		    	
		    	// we have to run a SA on the alpha/prob dhigh param
		    	
		    	SA = SA_ALPHA_INSPECTIONCOST;
		    }
		    			
	    }
	    
	    catch (ParseException exp ) {
	    	
	        // oops, something went wrong
	        System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
			log.log(Level.SEVERE, "Parsing failed.  Reason: " + exp.toString(), exp);
			
	    }
	    
	    	
        System.out.println("\n****** STARTING THE RUN OF THE EGT MODEL FOR TAX FRAUD (Feb. 2020)******\n");

        Date date = new Date();
        System.out.println("****** " + date.toString() + "******\n");

        File fileAllMC = new File ("./logs/" + "AllMCruns_" + params.getOutputFile() + ".txt");
        File fileSummaryMC = new File ("./logs/" + "SummaryMCruns_" +  params.getOutputFile() + ".txt");
        File fileAllMCLQ = new File ("./logs/" + "AllMCrunsLQ_" + params.getOutputFile() + ".txt");
        File fileSummaryMCLQ = new File ("./logs/" + "SummaryMCrunsLQ_" +  params.getOutputFile() + ".txt");
        File fileTimeSeriesMC = new File ("./logs/" + "TimeSeriesMCruns_" +  params.getOutputFile() + ".txt");
       
        // the SA check    	    			    	
	    if (SA == NO_SA) {
	    	
	    	// no SA, simple run
	    	
	    	RunStats stats;
	    	
	    	// print parameters for double-checking
	    	System.out.println("-> Parameters values:");
		    PrintWriter out = new PrintWriter(System.out, true);
	        params.printParameters(out);
	        
	        log.log(Level.FINE, "\n*** Parameters values of this model:\n" + params.export());
	        
	        
	        // START PREPARING CONTROLLER FOR RUNNING
			long time1 = System.currentTimeMillis ();
		
			Controller controller;
			
			controller = new Controller (params, paramsFile);
			
	        // END PREPARING CONTROLLER FOR RUNNING
	 		
			
			// BEGIN RUNNING MODEL WITH ALL THE MC SIMULATION
	 		stats = controller.runModel();		
	 		// END RUNNING MODEL WITH ALL THE MC SIMULATION
	 		
	 		stats.setExpName(params.getOutputFile());
	 		
	 		long  time2  = System.currentTimeMillis( );
	 		System.out.println("\n****** " + (double)(time2 - time1)/1000 + "s spent during the simulation");
	 		
	 		stats.calcAllStats();
	         
	 		// print the stats in the screen 		
	 		stats.printSummaryStats(out, false);
	 		stats.printSummaryStatsByAveragingLastQuartile(out, false);  // also the last quartile info
	 		System.out.println();
	 		
	 		// print the stats into a file
	        System.out.println("\n****** Stats also saved into a file ******\n");
	         
	        PrintWriter printWriter;
	         
	 		try {
	 			
	 			// print all the runs info into a file
	 			printWriter = new PrintWriter (fileAllMC);
	 			stats.printAllStats (printWriter, false);
	 	        printWriter.close (); 
	 	        
	 	        // print all the runs info (last quartiles of the sims) into a file
	 			printWriter = new PrintWriter (fileAllMCLQ);
	 			stats.printAllStatsByAveragingLastQuartile (printWriter, false);
	 	        printWriter.close ();  
	 	        
	 	        // print the summarized MC runs into a file
	 	        printWriter = new PrintWriter (fileSummaryMC);
	 			stats.printSummaryStats (printWriter, false);
	 	        printWriter.close ();    
	 	        
	 	        // print the summarized MC runs (last quartiles of the sims) into a file
	 	        printWriter = new PrintWriter (fileSummaryMCLQ);
	 			stats.printSummaryStatsByAveragingLastQuartile(printWriter, false);
	 	        printWriter.close ();    

	 	        // print the time series into a file
	 	        printWriter = new PrintWriter (fileTimeSeriesMC);
	 			stats.printTimeSeriesStats (printWriter);
	 	        printWriter.close ();    
	 	        
	 	        
	 		} catch (FileNotFoundException e) {
	 			
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 			
	 		    log.log( Level.SEVERE, e.toString(), e );
	 		} 
	    	
	    } else {
	    
	    	// SA over the initial population
	    	
	    	if (SA == SA_INITIAL_POPULATION) {
	    			    	    
	    		SensitivityAnalysis.runSA_kC_kD (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
	    		
	    	} else if ( SA == SA_ALPHA) {
	    		
	    		SensitivityAnalysis.runSA_alpha (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
	    		
	    	} else if ( SA == SA_REWARD) {
	    		
	    		SensitivityAnalysis.runSA_reward (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
	    		
	    	} else if ( SA == SA_FINE) {
	    		
	    		SensitivityAnalysis.runSA_fine (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
	    		
	    	} else if ( SA == SA_ALPHA_PROBDHIGH) {
	    		
	    		SensitivityAnalysis.runSA_alpha_probDHigh (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
	    		
	    	} else if ( SA == SA_ALPHA_INSPECTIONCOST) {
	    		
	    		SensitivityAnalysis.runSA_alpha_inspectionCost (params, paramsFile, fileAllMC, fileSummaryMC, fileAllMCLQ, fileSummaryMCLQ);
	    		
	    	}	     
	    }								
	}

	private static void setDLDHAndProbs(ModelParameters params) {

		// instead of setting DH and DL externally, we set in the following way:
		// DL is set to satisfy (1.71 < alpha*dL), when prob(alpha*dL)=0.2 (constant set in the code)
		// DH is 45.7588795207094 times higher than DL
			    	
	    //params.setValueForDL( (float) Math.ceil(1.71/ params.getAlpha()) );
		params.setValueForDL( 10 );
	    params.setValueForDH( (float) 45.7588 * params.getValueForDL() );

	    // generate prob from linear function depending on DH and DL
		params.setProbToBeInspected2AlphaDH( (float) ModelParameters.PROB_2ALPHA_DH );
		params.setProbToBeInspected2AlphaDL( (float) ModelParameters.PROB_2ALPHA_DL );
		
		try {
			
			if (params.getValueForDH() <= params.getValueForDL())
				
				// in case the parameter for dH and dL is the same we launch an error
	
				throw new IOException ("Error with dH and dL values. dH must be greater than dL. "
						+ "Check params dH and dL\n");
				
			else {
					
				params.setProbToBeInspectedAlphaDL( Functions.linearProbFunctionFromTwoPairs(2*params.getAlpha()*params.getValueForDL(), params.getProbToBeInspected2AlphaDL(), 
						2*params.getAlpha()*params.getValueForDH(), params.getProbToBeInspected2AlphaDH(), params.getAlpha()*params.getValueForDL()) );
				
				params.setProbToBeInspectedAlphaDH( Functions.linearProbFunctionFromTwoPairs(2*params.getAlpha()*params.getValueForDL(), params.getProbToBeInspected2AlphaDL(), 
						2*params.getAlpha()*params.getValueForDH(), params.getProbToBeInspected2AlphaDH(), params.getAlpha()*params.getValueForDH()) );
			}
			
		} catch (IOException e) {
		
			// TODO Auto-generated catch block
 			e.printStackTrace();
 			
 		    log.log( Level.SEVERE, e.toString(), e );
		}

	}	
	
}
