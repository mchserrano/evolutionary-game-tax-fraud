package view;

// package for std and mean

import java.io.PrintWriter;

import org.apache.commons.math3.stat.descriptive.*;


/** 
 * StatsRun
 * 
 * This class will store all the results for the MonteCarlo simulation.
 * It will also update the stats and error metrics w.r.t. the historical data.
 * 
 * @author mchica
 * @date 2020/02/03
 * @place Las Palmas GC
 *
 */
public class RunStats {

	private int numberRuns;				// number of MC simulations

	private int numberSteps;			// number of steps simulation
	
	// fields for k_C members of the population (time series)
	private int k_C[][];			// all the k_C members (each for run and step)
	private float avgk_C[];			// average k_C over all the runs for each step
	private float stdk_C[];			// std k_C over all the runs for each step
	private float[] min_k_C;
	private float[] max_k_C;
	
	// fields for k_D members of the population (time series)
	private int k_D[][];			// all the k_D members (each for run and step)
	private float avgk_D[];			// average k_D over all the runs for each step
	private float stdk_D[];			// std k_D over all the runs for each step
	private float[] min_k_D;
	private float[] max_k_D;
	

	// fields for netWealth
	private float netWealth[][];			// all the netWealth metrics (each for run and step)
	private float avgnetWealth[];			// average over all the runs for each step
	private float stdnetWealth[];			// std over all the runs for each step
	private float[] min_netWealth;
	private float[] max_netWealth;
	
	// fields for changes in the agents' strategies
	private int strategyChanges[][];			// the number of strategy changes in the agents (each for run and step)
	private float avgStrategyChanges[];			// average of the number of strategy changes over all the runs for each step
	private float stdStrategyChanges[];			// std of the number of strategy changes over all the runs for each step
	private float[] min_StrategyChanges;		// min of the number of strategy changes over all the runs for each step
	private float[] max_StrategyChanges;		// max of the number of strategy changes over all the runs for each step
				
	private String expName;				// the key name of this experiment (all the MC runs)
	
	
	// ########################################################################	
	// Methods/Functions 	
	// ########################################################################

	//--------------------------- Getters and setters ---------------------------//


	/**
	 * @return the numberRuns
	 */
	public int getNumberRuns() {
		return numberRuns;
	}

	/**
	 * @param _numberRuns the numberRuns to set
	 */
	public void setNumberRuns(int _numberRuns) {
		this.numberRuns = _numberRuns;
	}

	/**
	 * @return the stdk_D
	 */
	public float[] getStdk_D() {
		return stdk_D;
	}

	/**
	 * @param _stdk_D the stdk_D to set
	 */
	public void setStdk_D(float _stdk_D[]) {
		this.stdk_D= _stdk_D;
	}

	/**
	 * @return the avgk_D
	 */
	public float[] getAvgk_D() {
		return avgk_D;
	}

	/**
	 * @param _avgk_D the avgk_D to set
	 */
	public void setAvgk_D(float _avgk_D[]) {
		this.avgk_D = _avgk_D;
	}

	/**
	 * @return the k_D
	 */
	public int[][] getk_D() {
		return k_D;
	}

	/**
	 * @param _k_D the k_D to set
	 */
	public void setk_D(int _k_D[][]) {
		this.k_D = _k_D;
	}

	/**
	 * @return 
	 */
	public float[] getStdStrategyChanges() {
		return stdStrategyChanges;
	}

	/**
	 * @param _stdStrategyChanges the stdStrategyChanges to set
	 */
	public void setStdStrategyChanges(float[] _stdStrategyChanges) {
		this.stdStrategyChanges = _stdStrategyChanges;
	}

	/**
	 * @return the avgStrategyChanges
	 */
	public float[] getAvgStrategyChanges() {
		return avgStrategyChanges;
	}

	/**
	 * @param _avgStrategyChanges the avgStrategyChanges to set
	 */
	public void setAvgStrategyChanges(float _avgStrategyChanges[]) {
		this.avgStrategyChanges = _avgStrategyChanges;
	}

	/**
	 * @return the strategyChanges
	 */
	public int[][] getStrategyChanges() {
		return strategyChanges;
	}

	/**
	 * @param _StrategyChanges the StrategyChanges to set
	 */
	public void setStrategyChanges(int _StrategyChanges[][]) {
		this.strategyChanges = _StrategyChanges;
	}
		

	/**
	 * @return the stdnetWealth
	 */
	public float[] getStdnetWealth() {
		return stdnetWealth;
	}

	/**
	 * @param _stdnetWealth the stdnetWealth to set
	 */
	public void setStdnetWealth(float[] _stdnetWealth) {
		this.stdnetWealth = _stdnetWealth;
	}

	/**
	 * @return the avgnetWealth
	 */
	public float[] getAvgnetWealth() {
		return avgnetWealth;
	}
	
	public String getExpName() {
		return expName;
	}

	public void setExpName(String expName) {
		this.expName = expName;
	}

	/**
	 * @param _avgnetWealth the avgnetWealth to set
	 */
	public void setAvgnetWealth(float[] _avgnetWealth) {
		this.avgnetWealth = _avgnetWealth;
	}

	/**
	 * @return the netWealth
	 */
	public float[][] getnetWealth() {
		return netWealth;
	}

	/**
	 * @param _netWealth the netWealth to set for run _numberOfRun
	 */
	public void setnetWealthForRun(int _numberOfRun, float _netWealth[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.netWealth[_numberOfRun][i] = _netWealth[i];
	}
	
	/**
	 * @param _k_C k_C value to set for run _numberOfRun
	 */
	public void setk_CForRun(int _numberOfRun, int _k_C[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.k_C[_numberOfRun][i] = _k_C[i];
		
	}

	/**
	 * @param _k_D k_D value to set for run _numberOfRun
	 */
	public void setk_DForRun(int _numberOfRun, int _k_D[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.k_D[_numberOfRun][i] = _k_D[i];
		
	}

	/**
	 * 
	 * @param _numberOfRun number of the run to which we will set the changes in the strategies 
	 * @param _strategyChanges value of changing strategies to set for run _numberOfRun
	 */
	public void setStrategyChangesForRun(int _numberOfRun, int _strategyChanges[]) {
		
		for (int i = 0; i < this.numberSteps; i++)
			this.strategyChanges[_numberOfRun][i] = _strategyChanges[i];
		
	}
	
	/**
	 * @param _netWealth the netWealth to set
	 */
	public void setnetWealth(float _netWealth[][]) {
		
		this.netWealth = _netWealth;
	}
	
	/**
	 * @return the stdk_C
	 */
	public float[] getStdk_C() {
		return stdk_C;
	}

	/**
	 * @param _stdk_C the stdk_C to set
	 */
	public void setStdk_C(float[] _stdk_C) {
		this.stdk_C = _stdk_C;
	}

	/**
	 * @return the avgk_C
	 */
	public float[] getAvgk_C() {
		return avgk_C;
	}

	/**
	 * @param _avgk_C the avgk_C to set
	 */
	public void setAvgk_C(float _avgk_C[]) {
		this.avgk_C = _avgk_C;
	}

	/**
	 * @return the k_C
	 */
	public int[][] getk_C() {
		return k_C;
	}

	/**
	 * @param _k_C the k_C to set
	 */
	public void setk_C(int _k_C[][]) {
		this.k_C = _k_C;
	}
	
	/**
	 * @return the numberSteps
	 */
	public int getNumberSteps() {
		return numberSteps;
	}

	/**
	 * @param _numberSteps the numberSteps to set
	 */
	public void setNumberSteps(int _numberSteps) {
		this.numberSteps = _numberSteps;
	}
	
    
	//--------------------------- Constructor ---------------------------//
	/**
	 * constructor of Stats
	 * @param _nRuns
	 */
	public RunStats (int _nRuns, int _nSteps){

		numberRuns = _nRuns;
		numberSteps = _nSteps;
		
		// allocating space for metrics
		this.k_C = new int[_nRuns][_nSteps];
		this.k_D = new int[_nRuns][_nSteps];
		this.netWealth = new float[_nRuns][_nSteps];
		this.strategyChanges = new int[_nRuns][_nSteps];

		this.avgk_C = new float[_nSteps];	
		this.stdk_C = new float[_nSteps];
		this.min_k_C = new float[_nSteps];	
		this.max_k_C = new float[_nSteps];

		this.avgk_D = new float[_nSteps];	
		this.stdk_D = new float[_nSteps];
		this.min_k_D = new float[_nSteps];	
		this.max_k_D = new float[_nSteps];

		this.avgnetWealth = new float[_nSteps];	
		this.stdnetWealth = new float[_nSteps];
		this.min_netWealth = new float[_nSteps];	
		this.max_netWealth = new float[_nSteps];

		this.avgStrategyChanges = new float[_nSteps];	
		this.stdStrategyChanges = new float[_nSteps];
		this.min_StrategyChanges = new float[_nSteps];	
		this.max_StrategyChanges = new float[_nSteps];
		
	}
		
	/**
	 * This method prints all the steps values (avg and stdev of the MC RUNS) to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 */
	public void printTimeSeriesStats(PrintWriter writer) {
		
		writer.println("step;netWealthAvg;netWealthStd;netWealthMin;netWealthMax;"
				+ "k_CAvg;k_CStd;k_CMin;k_CMax;k_DAvg;k_DStd;k_DMin;k_DMax;" +
				"strategyChangesAvg;strategyChangesStd;strategyChangesMin;strategyChangesMax;");
		
		for (int i = 0; i < this.numberSteps; i++) {
			
			String toPrint = i + ";" 
					+ String.format("%.4f", this.avgnetWealth[i]) + ";" + String.format("%.4f", this.stdnetWealth[i]) + ";" 
					+ String.format("%.4f", this.min_netWealth[i]) + ";" + String.format("%.4f", this.max_netWealth[i]) + ";"
					+ String.format("%.4f", this.avgk_C[i]) + ";" + String.format("%.4f", this.stdk_C[i]) + ";" 
					+ String.format("%.4f", this.min_k_C[i]) + ";" + String.format("%.4f", this.max_k_C[i]) + ";"
					+ String.format("%.4f", this.avgk_D[i]) + ";" + String.format("%.4f", this.stdk_D[i]) + ";" 
					+ String.format("%.4f", this.min_k_D[i]) + ";" + String.format("%.4f", this.max_k_D[i]) + ";"
					+ String.format("%.4f", this.avgStrategyChanges[i]) + ";" + String.format("%.4f", this.stdStrategyChanges[i]) + ";" 
					+ String.format("%.4f", this.min_StrategyChanges[i]) + ";" + String.format("%.4f", this.max_StrategyChanges[i]) + ";\n";
					
			writer.print (toPrint);
		}
			
	}
		

	/**
	 * This method prints summarized stats (avg and std of MC runs) to a stream file (or to the console)
	 * @param append true if we append the line to an existing file, false if we destroy it first
	 * @param writer that is opened before calling the function
	 */
	public void printSummaryStats (PrintWriter writer, boolean append) {
		
		String toPrint = "keyNameExp;" + this.expName + ";netWealth;" 
				+ String.format("%.4f", this.avgnetWealth[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdnetWealth[(this.numberSteps - 1)]) + ";k_C;" 
				+ String.format("%.4f", this.avgk_C[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdk_C[(this.numberSteps - 1)]) 	+ ";k_D;" + 
				String.format("%.4f", this.avgk_D[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdk_D[(this.numberSteps - 1)]) 
				+ ";strategyChanges;" + String.format("%.4f", this.avgStrategyChanges[(this.numberSteps - 1)]) + ";" + 
				String.format("%.4f", this.stdStrategyChanges[(this.numberSteps - 1)]) + ";";
		
		if (append) {
			writer.append (toPrint);
			writer.append("\n");
		} else {
			writer.println (toPrint);
		}
					
			
	
	}
	
	/**
	 * This method prints (by appending to an existing file) the 
	 * summarized stats (avg and std of MC runs) to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 */
	public void appendSummaryStats (PrintWriter writer) {		
		printSummaryStats (writer, true);				
	}
	
	/**
	 * This method prints all the stats of the MC runs (by appending to an existing file) 
	 * to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 */
	public void appendAllStats (PrintWriter writer) {		
		printAllStats (writer, true);				
	}

	
	/**
	 * This method prints all the stats of the MC runs to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 * @param append true if we append the line to an existing file, false if we destroy it first
	 */
	public void printAllStats (PrintWriter writer, boolean append) {
		 			
		for (int i = 0; i < this.numberRuns; i++) {
			
			String toPrint = "keyNameExp;" + this.expName + ";MCrun;" + i + ";netWealth;" + 
					String.format("%.4f", this.netWealth[i][(this.numberSteps - 1)]) + ";k_C;" 
				+  String.format("%d", this.k_C[i][(this.numberSteps - 1)]) 
				+ ";k_D;" + String.format("%d", this.k_D[i][(this.numberSteps - 1)]) + ";strategyChanges;" 
				+ String.format("%d", this.strategyChanges[i][(this.numberSteps - 1)]) + ";\n";
		
			
			if (append) 	
				writer.append (toPrint);				
			else 					
				writer.print (toPrint);					
		}	
	}
	
	/**
	 * This method prints summarized stats of the last quartile of the simulation (avg and std of MC runs) 
	 * to a stream file (or to the console)
	 * @param append true if we append the line to an existing file, false if we destroy it first
	 * @param writer that is opened before calling the function
	 */
	public void printSummaryStatsByAveragingLastQuartile (PrintWriter writer, boolean append) {
		
		double lastQuartileNW = 0.;
		double lastQuartileK_C = 0.;
		double lastQuartileK_D = 0.;
		double lastQuartileStrategyChanges = 0.;

		double lastQuartileNWStd = 0.;
		double lastQuartileK_CStd = 0.;
		double lastQuartileK_DStd = 0.;
		double lastQuartileStrategyChangesStd = 0.;
		
		// check the number of steps which means the last 25% of them
		int quartileSteps = (int) Math.round(0.25*this.numberSteps);
		
		for ( int j = 1; j <= quartileSteps; j++) {
			
			// averaging the MC outputs for the last quartile of the simulation
			lastQuartileNW += this.avgnetWealth[(this.numberSteps - j)];
			lastQuartileK_C += this.avgk_C[(this.numberSteps - j)];
			lastQuartileK_D += this.avgk_D[(this.numberSteps - j)];
			lastQuartileStrategyChanges += this.avgStrategyChanges[(this.numberSteps - j)];
			
			// averaging the MC stdev outputs for the last quartile of the simulation
			lastQuartileNWStd += this.stdnetWealth[(this.numberSteps - j)];
			lastQuartileK_CStd += this.stdk_C[(this.numberSteps - j)];
			lastQuartileK_DStd += this.stdk_D[(this.numberSteps - j)];
			lastQuartileStrategyChangesStd += this.stdStrategyChanges[(this.numberSteps - j)];			
			
		}

		lastQuartileNW /= quartileSteps;
		lastQuartileK_C /= quartileSteps;
		lastQuartileK_D /= quartileSteps;
		lastQuartileStrategyChanges /= quartileSteps;
		
		lastQuartileNWStd /= quartileSteps;
		lastQuartileK_CStd /= quartileSteps;
		lastQuartileK_DStd /= quartileSteps;
		lastQuartileStrategyChangesStd /= quartileSteps;
		
		String toPrint = "LQkeyNameExp;" + this.expName + ";netWealthLQ;" 
				+ String.format("%.4f", lastQuartileNW) + ";" + String.format("%.4f", lastQuartileNWStd) + ";k_CLQ;" 
				+ String.format("%.4f", lastQuartileK_C) + ";" + String.format("%.4f", lastQuartileK_CStd) 
				+ ";k_DLQ;" + String.format("%.4f", lastQuartileK_D) + ";" + String.format("%.4f", lastQuartileK_DStd) 
				 + ";strategyChangesLQ;" + String.format("%.4f", lastQuartileStrategyChanges) + ";" + String.format("%.4f", lastQuartileStrategyChangesStd) + ";";
				
		if (append) {
			writer.append (toPrint);
			writer.append("\n");
		} else {
			writer.println (toPrint);
		}	
			
	}
	
	
	/**
	 * This method prints all the averaging stats in the last quartile of the simulation for all 
	 * the MC runs to a stream file (or to the console)
	 * @param writer that is opened before calling the function
	 * @param append true if we append the line to an existing file, false if we destroy it first
	 */
	public void printAllStatsByAveragingLastQuartile (PrintWriter writer, boolean append) {
		
		for ( int i = 0; i < this.numberRuns; i++) {
			
			double lastQuartileNW = 0.;
			double lastQuartileK_C = 0.;
			double lastQuartileK_D = 0.;
			double lastQuartileStrategyChanges = 0.;
			
			// check the number of steps which means the last 25% of them
			int quartileSteps = (int)Math.round(0.25*this.numberSteps);
			
			for ( int j = 1; j <= quartileSteps; j++) {
				lastQuartileNW += this.netWealth[i][(this.numberSteps - j)];
				lastQuartileK_C += this.k_C[i][(this.numberSteps - j)];
				lastQuartileK_D += this.k_D[i][(this.numberSteps - j)];
				lastQuartileStrategyChanges += this.strategyChanges[i][(this.numberSteps - j)];
			}

			lastQuartileNW /= quartileSteps;
			lastQuartileK_C /= quartileSteps;
			lastQuartileK_D /= quartileSteps;
			lastQuartileStrategyChanges /= quartileSteps;
			
			String toPrint = "LQKeyNameExp;" + this.expName + ";MCrun;" + i + ";netWealthLQ;" + 
					String.format("%.4f", lastQuartileNW) + ";k_CLQ;" 
					+  String.format("%.4f", lastQuartileK_C) 
					+ ";k_DLQ;" + String.format("%.4f", lastQuartileK_D) + ";strategyChangesLQ;" 
					+ String.format("%.4f", lastQuartileStrategyChanges) + ";\n";
					
			if (append) 
				writer.append (toPrint);		
			else 
				writer.print (toPrint);					
			
		}	
		
	}
	
	
	/**
	 * This method is for calculating all the statistical information for 
	 * the runs of the metrics
	 * 	 
	 */
	public void calcAllStats () {
					
		for( int j = 0; j < this.numberSteps; j++) {
			
			// Get a DescriptiveStatistics instance
			DescriptiveStatistics statsk_C = new DescriptiveStatistics();
			DescriptiveStatistics statsk_D = new DescriptiveStatistics();
			DescriptiveStatistics statsnetWealth = new DescriptiveStatistics();
			DescriptiveStatistics statsStrategyChanges = new DescriptiveStatistics();
			
			// Add the data from the array
			for( int i = 0; i < this.numberRuns; i++) {
				
		        statsk_C.addValue(this.k_C[i][j]);
		        statsk_D.addValue(this.k_D[i][j]);
		        
		        statsnetWealth.addValue(this.netWealth[i][j]);	  
		        
		        statsStrategyChanges.addValue(this.strategyChanges[i][j]);	 		        
		        
			}

			// calc mean and average for all of them
			
			this.avgk_C[j] = (float)statsk_C.getMean();	
			this.stdk_C[j] = (float)statsk_C.getStandardDeviation();
			this.min_k_C[j] = (float)statsk_C.getMin();	
			this.max_k_C[j] = (float)statsk_C.getMax();
			
			this.avgk_D[j] = (float)statsk_D.getMean();	
			this.stdk_D[j] = (float)statsk_D.getStandardDeviation();
			this.min_k_D[j] = (float)statsk_D.getMin();	
			this.max_k_D[j] = (float)statsk_D.getMax();

			this.avgnetWealth[j] = (float)statsnetWealth.getMean();	
			this.stdnetWealth[j] = (float)statsnetWealth.getStandardDeviation();
			this.min_netWealth[j] = (float)statsnetWealth.getMin();	
			this.max_netWealth[j] = (float)statsnetWealth.getMax();

			this.avgStrategyChanges[j] = (float)statsStrategyChanges.getMean();	
			this.stdStrategyChanges[j] = (float)statsStrategyChanges.getStandardDeviation();
			this.min_StrategyChanges[j] = (float)statsStrategyChanges.getMin();	
			this.max_StrategyChanges[j] = (float)statsStrategyChanges.getMax();
		}				
	}

}
