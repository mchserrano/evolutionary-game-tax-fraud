package model;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import org.graphstream.algorithm.Toolkit;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSourceDGS;

import socialnetwork.GraphStreamer.NetworkType;
import util.*;


/**
 * Parameters of the model of evolutionary dynamics for the game of tax fraud
 * 
 * @author mchica
 * @date 2020/02/03
 * @place Las Palmas GC
 *
 */

public class ModelParameters {

	// Read configuration file
	ConfigFileReader config;
	
	// CONFIGURATION TO CALCULATE, STORE STATISTICS	
	public static boolean OUTPUT_WHOLE_EVOLUTION = false; 
	public static boolean OUTPUT_STRENGTH_STRATEGIES = false;
	
	// FOR THE STRATEGY TYPE FOR AGENTS
	public final static int UNDEFINED_STRATEGY = 0;
	public final static int COOPERATOR = 1;
	public final static int DEFECTOR = 2;

	public final static int WELL_MIXED_POP = -1;
	public final static int SF_NETWORK = 0;
	public final static int ER_NETWORK = 1;
	public final static int SW_NETWORK = 2;
	public final static int PGP_NETWORK = 3;
	public final static int EMAIL_NETWORK = 4;
	public final static int REGULAR_NETWORK = 5;

	// TYPE OF UPDATE RULE FOR THE AGENTS
	public final static int PROPORTIONAL_UPDATE_RULE = 1;
	public final static int UI_UPDATE_RULE = 2;
	public final static int VOTER_UPDATE_RULE = 3;
	public final static int FERMI_UPDATE_RULE = 4;
	public final static int WF_UPDATE_RULE = 5;  			 // Wright-Fisher, previously called Moran in other projects

	// AGGREGATION METHOD FOR PAYOFF CALCULATION
	public final static String PAYOFF_AGGREGATION = "ADD";   // MAX, ADD (standard way), AVG are the values
	
	// CONSTANTS WHICH ARE SPECIFIC FOR THIS TAX FRAUD GAME
	public final static double PROB_2ALPHA_DH = 0.5;
	public final static double PROB_2ALPHA_DL = 0.5;

	
	// ########################################################################
	// Variables
	// ########################################################################

	String outputFile;

	// modified just to use a static SN from a file

	// FILE_NETWORK ONLY!!
	// Choose between several social networks:
	// SCALE_FREE_NETWORK, scale-free (Barabasi)
	// ER_RANDOM_NETWORK
	// SW_RANDOM_NETWORK
	NetworkType typeOfNetwork;

	String networkFilesPattern;

	// graph read from file
	Graph graphFromFile;

	
	//////////////////////////////////////////////7
	
	
	// parameters for the payoff calculation 
	float fine; 					// fine to be applied when a defector is inspected
	float alpha; 					// alpha which is a percentage \in [0,1] meaning the non-declared amount in the transaction
	float inspectionCost; 			// inspection cost
	float reputationalReward; 		// reputational reward for Cs
	float gammaSocialReputation; 	// gamma to add linear combination of social comparison for reputational reward

	float valueForDH; 				// value for high values to be declared
	float valueForDL; 				// value for low values to be declared


	float probToBeInspectedAlphaDH;		// prob to be inspected when having alphaDH in a transaction
	float probToBeInspectedAlphaDL;		// prob to be inspected when having alphaDL in a transaction
	float probToBeInspected2AlphaDH;	// prob to be inspected when having 2alphaDH in a transaction
	float probToBeInspected2AlphaDL;	// prob to be inspected when having 2alphaDL in a transaction
		

	// population distribution
	int nrAgents;

	float percentageCooperators; 	// percentage of agents to be cooperators at the
									// beginning of the simulation w.r.t. total of players (defectors = 100% - percentageCooperators)
	
	float probDHigh; 				// probability of links in the social network to have high value declarations
									// the rest of the links will have low value

	int k_C; 						// no. of agents to be cooperators at the beginning of the sim
	int k_D; 						// no. of agents to be defectors at the beginning of the sim
	
	
	// update's rule for the ev dynamics game
	int T_rounds = 1; 				// number of steps to update the strategy of the agents by an update rule
	
	int maxSteps; 					// maximum number of steps for the simulation
	int runsMC; 					// number of MC runs
	int updateRule; 				// identifier for the update rule of the agents

	float q_VM = 1;					// q value in [0,1] to either choose the VM or UI. If 1, always VM. 1-q, UI is applied

	long seed; 						// seed to run all the simulations
	
	double mutProb;					// probability to mutate a strategy
	
	// --------------------------- Get/Set methods ---------------------------//
	//
	
	/**
	 * @return the mutProb
	 */
	public double getMutProb() {
		return mutProb;
	}

	/** 
	 * 
	 * @param mutProb 
	 */
	public void setMutProb(double _mutProb) {
		this.mutProb = _mutProb;
	}
	
	
	public float getProbToBeInspectedAlphaDH() {
		return probToBeInspectedAlphaDH;
	}

	public void setProbToBeInspectedAlphaDH(float _prob) {
		this.probToBeInspectedAlphaDH = _prob;
	}
	
	public float getProbToBeInspectedAlphaDL() {
		return probToBeInspectedAlphaDL;
	}

	public void setProbToBeInspectedAlphaDL(float _prob) {
		this.probToBeInspectedAlphaDL = _prob;
	}

	public float getProbToBeInspected2AlphaDH() {
		return probToBeInspected2AlphaDH;
	}

	public void setProbToBeInspected2AlphaDH(float _prob) {
		this.probToBeInspected2AlphaDH = _prob;
	}
	
	public float getProbToBeInspected2AlphaDL() {
		return probToBeInspected2AlphaDL;
	}

	public void setProbToBeInspected2AlphaDL(float _prob) {
		this.probToBeInspected2AlphaDL = _prob;
	}

	/**
	 * @return the gamma value to add social influence for reputation (C players)
	 */
	public float getGammaSocialReputation() {
		return gammaSocialReputation;
	}
	
	/** 
	 * 
	 * @param _gammaSocialReputation the gamma value to add social influence for reputation (C players)
	 */
	public void setGammaSocialReputation(float _gamma) {
		this.gammaSocialReputation = _gamma;
	}
	
	/**
	 * @return the q value for choosing between VM or UI
	 */
	public float getQ_VM() {
		return q_VM;
	}

	/** 
	 * 
	 * @param q_VM THE q value for choosing between VM or UI
	 */
	public void setQ_VM(float q_VM) {
		this.q_VM = q_VM;
	}
	
	/**
	 * @return the seed for running all the simulations
	 */
	public long getSeed() {
		return seed;
	}

	/**
	 * @param the
	 *            seed to run all the simulation
	 */
	public void setSeed(long _seed) {
		this.seed = _seed;
	}

	/**
	 * @return the max number of steps of the simulation
	 */
	public int getMaxSteps() {
		return maxSteps;
	}

	/**
	 * @param the max number of steps of the simulation
	 */
	public void setMaxSteps(int _maxSteps) {
		this.maxSteps = _maxSteps;
	}

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	/**
	 * @return the alpha value (defection rate from the total amount to be declared)
	 */
	public float getAlpha() {
		return this.alpha;
	}

	/**
	 * @param _a the value for alpha
	 */
	public void setAlpha(float _a) {
		this.alpha = _a;
	}

	/**
	 * @return the number of rounds to use the update rule
	 */
	public int getT_rounds() {
		return T_rounds;
	}

	/**
	 * @param _T_rounds
	 *            is the number of rounds to use the update rule
	 */
	public void setT_rounds(int _T_rounds) {
		this.T_rounds = _T_rounds;
	}

	/**
	 * @return the typeOfNetwork
	 */
	public NetworkType getTypeOfNetwork() {
		return typeOfNetwork;
	}

	/**
	 * @param typeOfNetwork
	 *            the typeOfNetwork to set
	 */
	public void setTypeOfNetwork(NetworkType typeOfNetwork) {
		this.typeOfNetwork = typeOfNetwork;
	}

	/**
	 * @return the graph
	 */
	public Graph getGraph() {
		return graphFromFile;
	}

	/**
	 * @param _graph
	 *            to set
	 */
	public void setGraph(Graph _graph) {
		this.graphFromFile = _graph;
	}

	public String getNetworkFilesPattern() {
		return networkFilesPattern;
	}

	public void setNetworkFilesPattern(String networkFilesPattern) {
		this.networkFilesPattern = networkFilesPattern;
	}

	/**
	 * @param _graph
	 *            to set
	 * @throws IOException
	 */
	public void readGraphFromFile(String fileNameGraph) throws IOException {

		FileSourceDGS fileSource = new FileSourceDGS();
		graphFromFile = new SingleGraph("SNFromFile");

		fileSource.addSink(graphFromFile);
		fileSource.readAll(fileNameGraph);

		fileSource.removeSink(graphFromFile);
		
		//this.displayGraph();
		
		/*System.out.println("number of nodes: " + graphFromFile.getNodeCount());

		int distr[] = Toolkit.degreeDistribution(graphFromFile);
		
		for (int k = 0; k < distr.length; k++)
			System.out.println("degree "  + k + "; " + distr[k]);

		System.out.println(Toolkit.averageClusteringCoefficient(graphFromFile));
		System.out.println(Toolkit.density(graphFromFile));
		System.out.println(Toolkit.diameter(graphFromFile));
		System.out.println(Toolkit.averageDegree(graphFromFile));*/
	}

	/**
	 * Display the graph
	 * 
	 * @return
	 */
	public void displayGraph (Set<Integer> k_I, Set <Integer> k_T, Set<Integer> k_U) {
	
		for (Node node : graphFromFile) {
			

			node.addAttribute("ui.color",3);
			node.setAttribute("size", "medium");
			//node.setAttribute("ui:fill-color", "blue");
			node.addAttribute("ui.label", node.getId());
			
			if (k_I.contains(Integer.getInteger(node.getId()))) {

				node.setAttribute("ui.color", 0); // The node will be green.
				
			} else if (k_T.contains(Integer.getInteger(node.getId()))) {

				node.setAttribute("ui.color", 0.5); // The node will be a mix of green and red.
				
			} else if (k_U.contains(Integer.getInteger(node.getId()))) {

				node.setAttribute("ui.color", 1); // The node will be red.
			}
			
		}
	
		graphFromFile.display();	
	}
	
	
	/**
	 * Gets the number of agents.
	 * 
	 * @return
	 */
	public int getNrAgents() {
		return nrAgents;
	}

	/**
	 * Sets the number of agents
	 * 
	 * @param nrAgents
	 */
	public void setNrAgents(int nrAgents) {
		if (nrAgents > 0)
			this.nrAgents = nrAgents;
	}

	/**
	 * Gets the type of update rule for the agents.
	 * 
	 * @return
	 */
	public int getUpdateRule() {
		return this.updateRule;
	}

	/**
	 * Sets the update rule.
	 * 
	 * @param updateRule
	 */
	public void setUpdateRule(int _updateRule) {
		this.updateRule = _updateRule;
	}

	/**
	 * Gets inspection cost
	 * 
	 * @return
	 */
	public float getInspectionCost() {
		return this.inspectionCost;
	}

	/**
	 * Sets inspection cost
	 * 
	 * @param _ins the value for the inspection ost
	 */
	public void setInspectionCost(float _ins) {
		this.inspectionCost = _ins;
	}

	/**
	 * Gets reputational reward
	 * 
	 * @return
	 */
	public float getReputationalReward() {
		return this.reputationalReward;
	}

	/**
	 * Sets reputational reward
	 * 
	 * @param _rep is the reward for Cs
	 */
	public void setReputationalReward(float _rep) {
		this.reputationalReward = _rep;
	}

	/**
	 * Gets fine for Ds
	 * 
	 * @return
	 */
	public float getFine() {
		return this.fine;
	}

	/**
	 * Sets fine for Ds
	 * 
	 * @param _fine
	 */
	public void setFine(float _fine) {
		this.fine = _fine;
	}

	/**
	 * Gets percentage of Cs
	 * 
	 * @return the percentages of Cs w.r.t. all the population
	 */
	public float getPercentageCooperators() {
		return this.percentageCooperators;
	}

	/**
	 * Sets percentage of Cs
	 * 
	 * @param percentageCooperators
	 */
	public void setPercentageCooperators(float _per) {
		this.percentageCooperators = _per;
	}

	/**
	 * Gets prob of links in the SN to have high amount to be declared
	 * 
	 * @return the prob for having high value declarations
	 */
	public float getProbDHigh() {
		return this.probDHigh;
	}

	/**
	 * Sets the prob for having high value declarations
	 * 
	 * @param probDHigh
	 */
	public void setProbDHigh(float _dH) {
		this.probDHigh= _dH;
	}
	/**
	 * Gets k_C (number of Cs).
	 * 
	 * @return
	 */
	public int getk_C() {
		return this.k_C;
	}

	/**
	 * Sets k_C (number of Cs).
	 * 
	 * @param _k_C
	 */
	public void setk_C(int _k_C) {
		this.k_C = _k_C;
	}

	/**
	 * Gets k_D (number of Ds).
	 * 
	 * @return
	 */
	public int getk_D() {
		return this.k_D;
	}

	/**
	 * Sets k_D (number of Ds).
	 * 
	 * @param _k_D
	 */
	public void setk_D(int _k_D) {
		this.k_D = _k_D;
	}

	/**
	 * Gets value for DHigh
	 * 
	 * @return
	 */
	public float getValueForDH() {
		return this.valueForDH;
	}

	/**
	 * Sets value for DHigh
	 * 
	 * @param _high
	 */
	public void setValueForDH(float _high) {
		this.valueForDH = _high;
	}

	/**
	 * Gets value for DLow
	 * 
	 * @return
	 */
	public float getValueForDL() {
		return this.valueForDL;
	}

	/**
	 * Sets value for DLow
	 * 
	 * @param _low
	 */
	public void setValueForDL(float _low) {
		this.valueForDL = _low;
	}
	
	/**
	 * @return number of MC runs
	 */
	public int getRunsMC() {
		return runsMC;
	}

	/**
	 * Sets number of MC runs
	 * 
	 * @param _runsMC
	 */
	public void setRunsMC(int _runsMC) {
		this.runsMC = _runsMC;
	}

	// ########################################################################
	// Constructors
	// ########################################################################

	/**
	 * 
	 */
	public ModelParameters() {

	}

	// ########################################################################
	// Export methods
	// ########################################################################

	public String export() {

		String values = "";

		values += exportGeneral();

		values += exportSN();

		return values;
	}

	private String exportSN() {

		String result = "";

		result += "SNFile = " + this.networkFilesPattern + "\n";
		result += "typeOfSN = " + this.typeOfNetwork + "\n";

		return result;

	}

	private String exportGeneral() {

		String result = "";

		result += "MC_runs = " + this.runsMC + "\n";
		result += "seed = " + this.seed + "\n";

		result += "nrAgents = " + this.nrAgents + "\n";

		result += "maxSteps = " + this.maxSteps + "\n";
		result += "T_rounds = " + this.T_rounds + "\n";

		result += "percentage_cooperators = " + this.percentageCooperators + "\n";
		result += "prob_dH = " + this.probDHigh + "\n";

		result += "alpha = " + this.alpha + "\n";
		result += "reputationalReward = " + this.reputationalReward + "\n";
		result += "gammaReputationalReward = " + this.gammaSocialReputation + "\n";
		result += "fine = " + this.fine + "\n";

		result += "inspectionCost = " + this.inspectionCost + "\n";
		result += "d_H = " + this.valueForDH + "\n";
		result += "d_L = " + this.valueForDL + "\n";
		result += "prob. to be inspected (calculated from alpha*d_L) = " + this.probToBeInspectedAlphaDL + "\n";
		result += "prob. to be inspected (calculated from alpha*d_H) = " + this.probToBeInspectedAlphaDH + "\n";
		result += "prob. to be inspected (calculated from 2*alpha*d_L) = " + this.probToBeInspected2AlphaDL + "\n";
		result += "prob. to be inspected (calculated from 2*alpha*d_H) = " + this.probToBeInspected2AlphaDH + "\n";

		result += "mutProb = " + this.mutProb + "\n";

		if (this.updateRule == ModelParameters.PROPORTIONAL_UPDATE_RULE)
			result += "updateRule = PROPORTIONAL_UPDATE_RULE\n";
		
		else if (this.updateRule == ModelParameters.UI_UPDATE_RULE)
			result += "updateRule = UI_UPDATE_RULE\n";
		
		else if (this.updateRule == ModelParameters.VOTER_UPDATE_RULE) {
			result += "updateRule = VOTER_UPDATE_RULE\n";
			result += "q_VM = " + this.q_VM + "\n";
		}
					
		else if (this.updateRule == ModelParameters.FERMI_UPDATE_RULE)
			result += "updateRule = FERMI_UPDATE_RULE\n";
		
		else if (this.updateRule == ModelParameters.WF_UPDATE_RULE)
			result += "updateRule = WF_UPDATE_RULE\n";
			
		return result;
	}

	/** 
	 * 
	 */
	public void updateKsFromPercentages() {

		this.k_C = (int) Math.round(this.nrAgents * this.percentageCooperators);
		this.k_D = this.nrAgents - this.k_C;

	}
		
	/**
	 * Reads parameters from the configuration file.
	 */
	public void readParameters(String CONFIGFILENAME) {

		try {

			// Read parameters from the file
			config = new ConfigFileReader(CONFIGFILENAME);

			config.readConfigFile();

			// Get global parameters
			this.maxSteps = config.getParameterInteger("maxSteps");
			this.runsMC = config.getParameterInteger("MCRuns");
			this.T_rounds = config.getParameterInteger("T_rounds");
			this.seed = config.getParameterInteger("seed");

			this.nrAgents = config.getParameterInteger("nrAgents");

			// obtain the percentage of dH and dL
			this.probDHigh = (float) config.getParameterDouble ("prob_dHigh");
			
			// obtain the percentage of Cs/Ds to calculate k_C, k_D
			this.percentageCooperators = (float) config.getParameterDouble ("percentage_cooperators");

			if ((this.percentageCooperators > 1.0) || (this.percentageCooperators < 0)) {
				throw new IOException("Error with % of cooperators. It is > 1 or < 0. "
						+ "Check params percentageCooperators\n");
			}

			this.updateKsFromPercentages();

			this.updateRule = config.getParameterInteger("updateRule");

			if (this.updateRule == ModelParameters.VOTER_UPDATE_RULE) {
				
				try {
					this.q_VM = (float) config.getParameterDouble("q_VM");
				} catch (Exception e) {
					
					// if it is not defined, q by default (= 1). Always VM
					this.q_VM = (float) 1.;
				}					
				
			} else {
				this.q_VM = -1;
			}
			
			this.alpha = (float) config.getParameterDouble("alpha");
			this.reputationalReward = (float) config.getParameterDouble("reputationalReward");
			this.fine = (float) config.getParameterDouble("fine");
			this.inspectionCost = (float) config.getParameterInteger("inspectionCost");
			this.gammaSocialReputation = (float) config.getParameterInteger("gammaSocialReputation");
			
			this.mutProb = config.getParameterDouble("mutProb");
			
			//this.valueForDH = (float) config.getParameterInteger("dH");
			//this.valueForDL = (float) config.getParameterInteger("dL");
					
					
			// Always read social network file but this file can be SF, Random,
			// RW or regular

			setNetworkFilesPattern(config.getParameterString("SNFile"));

			this.readGraphFromFile(networkFilesPattern);

			if (config.getParameterInteger("typeOfNetwork") == SW_NETWORK) {

				typeOfNetwork = NetworkType.SW_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == ER_NETWORK) {

				typeOfNetwork = NetworkType.RANDOM_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == SF_NETWORK) {

				typeOfNetwork = NetworkType.SCALE_FREE_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == WELL_MIXED_POP) {

				typeOfNetwork = NetworkType.WELL_MIXED_POP;
			}
			if (config.getParameterInteger("typeOfNetwork") == PGP_NETWORK) {

				typeOfNetwork = NetworkType.PGP_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == EMAIL_NETWORK) {

				typeOfNetwork = NetworkType.EMAIL_NETWORK;
			}
			if (config.getParameterInteger("typeOfNetwork") == REGULAR_NETWORK) {

				typeOfNetwork = NetworkType.REGULAR_NETWORK;
			}

		} catch (IOException e) {

			System.err.println("Error with SN file when loading parameters for the simulation " + CONFIGFILENAME + "\n"
					+ e.getMessage());
			e.printStackTrace(new PrintWriter(System.err));
		}
	}

	// ----------------------------- I/O methods -----------------------------//

	/**
	 * Prints simple statistics evolution during the time.
	 */
	public void printParameters(PrintWriter writer) {

		// printing general params
		writer.println(this.export());

	}

	
}
