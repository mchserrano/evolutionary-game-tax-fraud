package model;

import sim.engine.*;

import java.util.*;


/**
 * Class of the players of the EGT model for tax fraud
 * 
 * @author mchica
 * @date 2020/02/03
 * @place Las Palmas GC
 *
 */

public class GamerAgent implements Steppable {

	// ########################################################################
	// Variables
	// ########################################################################


	private static final long serialVersionUID = 1L;

	// --------------------------------- Fixed -------------------------------//

	int gamerAgentId; 	// A unique agent Id

	int maxSteps; 		// max number of steps of the simulation of the agent

	int T_rounds; 		// rounds of T steps to update the rule

	int currentStep; 	// the current step of the simulation


	// ------------------------------- Dynamic -------------------------------//

	float currentPayoff = Float.MIN_VALUE; // payoff obtained by the agent at this step
	float previousPayoff = Float.MIN_VALUE; // payoff obtained by the agent previous step

	byte currentStrategy = ModelParameters.UNDEFINED_STRATEGY; // current strategy used by the agent
	byte previousStrategy = ModelParameters.UNDEFINED_STRATEGY; // strategy used by the agent in the previous step

	// ########################################################################
	// Constructors
	// ########################################################################

	/**
	 * Initializes a new instance of the ClientAgent class.
	 * 
	 * @param _gameId   is the id for the agent
	 * @param _strategy is the strategy used at the beginning of the simulation
	 * @param _prob     the probability to act at each step of the simulation
	 * @param _maxSteps the max steps of the simulation
	 * @param _T_rounds the T rounds to use the update rule
	 * @param _d_Value  the value to declare, high or low
	 * 
	 */
	public GamerAgent(int _gamerId, byte _strategy, int _maxSteps, int _T_rounds, float _d_Value) {

		this.gamerAgentId = _gamerId;
		this.currentStrategy = _strategy;
		this.maxSteps = _maxSteps;
		this.T_rounds = _T_rounds;	

	}

	// ########################################################################
	// Methods/Functions
	// ########################################################################

	// --------------------------- Get/Set methods ---------------------------//

	/**
	 * Gets the id of the agent
	 * 
	 * @return
	 */
	public int getGamerAgentId() {
		return gamerAgentId;
	}

	/**
	 * Sets the id of the agent
	 * 
	 * @param gamerAgentId
	 */
	public void setGamerAgentId(int _gamerAgentId) {
		this.gamerAgentId = _gamerAgentId;
	}

	/**
	 * Gets the current payoff of the agent
	 * 
	 * @return - the payoff
	 */
	public float getCurrentPayoff() {
		return this.currentPayoff;
	}

	/**
	 * Sets the current payoff of the agent
	 * 
	 * @param _strategy - the current payoff to be set
	 */
	public void setCurrentPayoff(float _p) {
		this.currentPayoff = _p;
	}

	/**
	 * Gets the previous payoff of the agent
	 * 
	 * @return - the payoff
	 */
	public float getPreviousPayoff() {
		return this.previousPayoff;
	}

	/**
	 * Sets the previous payoff of the agent
	 * 
	 * @param _strategy - the current payoff to be set
	 */
	public void setPreviousPayoff(float _strategy) {
		this.previousPayoff = _strategy;
	}

	/**
	 * Gets the current strategy of the agent
	 * 
	 * @return - the strategy
	 */
	public byte getCurrentStrategy() {
		return this.currentStrategy;
	}

	/**
	 * Sets the strategy status of the agent
	 * 
	 * @param _strategy - the current strategy to be set
	 */
	public void setCurrentStrategy(byte _strategy) {
		this.currentStrategy = _strategy;
	}

	/**
	 * Gets the previous strategy of the agent
	 * 
	 * @return - the strategy
	 */
	public byte getPreviousStrategy() {
		return this.previousStrategy;
	}

	/**
	 * Sets the previous strategy of the agent
	 * 
	 * @param _strategy - the current strategy to be set
	 */
	public void setPreviousStrategy(byte _strategy) {
		this.previousStrategy = _strategy;
	}

	/**
	 * Returns true if the agent has changed its strategy this step. False if it has
	 * the same
	 */
	public boolean hasChangedStrategyAtStep(int _step) {

		if ((_step > 0) && (this.getCurrentStrategy() != this.getPreviousStrategy()) )
			return true;
		else
			return false;
		
	}


	// -------------------------- Cooperation methods --------------------------//

	/**
	 * This function is called to calculate the payoffs of the play of the agent
	 * within its local neighbourhood of the SN. The payoff is calculated following
	 * the payoff matrix of the defined game for tax fraud (see corresponding paper).
	 * 
	 * It is a pairwise game. Depending on the configuration, we average/add/max 
	 * all the interactions of the focal agent to set its payoff value
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return the payoff of the agent in this step
	 */
	public double calculatePayoffWithNeighbors(SimState state) {

		Model model = (Model) state;

		ArrayList<Integer> neighbors = (ArrayList<Integer>) model.socialNetwork.getNeighborsOfNode(this.gamerAgentId);

		// setting the payoff to the agent according to the utility matrix

		// TODO CUSTOMIZE WAY OF AGGREGATION
		if (ModelParameters.PAYOFF_AGGREGATION.compareTo("MAX") == 0) {
			this.currentPayoff = (float) Float.MIN_VALUE;
		} else {
			this.currentPayoff = (float) 0;
		}

		
		// Iterate over neighbors

		for (int i = 0; i < neighbors.size(); i++) {

			GamerAgent neighbor = (GamerAgent) (model.getAgents()).get(neighbors.get(i));
			
			// get DH or DL from the edge between the focal agent and its neighbor
			String valueEdge = (String) (model.getSocialNetwork().getAttributeEdge
					(this.gamerAgentId, neighbor.getGamerAgentId(), "d_value"));
					
			//System.out.print("d_value = " + valueEdge + "; ");

			float payoffWithNeighbor = 0;
			
			// check the i-th neighbor strategy and the focal agent's strategy to calculate payoffs

			if (neighbor.getCurrentStrategy() == ModelParameters.COOPERATOR) {
				
				if (this.currentStrategy == ModelParameters.COOPERATOR) {
					
					// CASE C VS. C (= reputational reward)			
					payoffWithNeighbor = model.getParametersObject().getReputationalReward();

					//System.out.println("C vs C. Payoff is " + payoffWithNeighbor);

				} else if (this.currentStrategy == ModelParameters.DEFECTOR) {
															
					// CASE D VS. C (= saving alpha - inspection cost - probability to be fined)
					
					float dValue, probFromAlphaDValue;

					if (valueEdge.equals("high")) {
						probFromAlphaDValue = model.getParametersObject().getProbToBeInspectedAlphaDH();
						dValue = model.getParametersObject().getValueForDH();
					} else {
						probFromAlphaDValue = model.getParametersObject().getProbToBeInspectedAlphaDL();
						dValue = model.getParametersObject().getValueForDL();
					}

					payoffWithNeighbor = model.getParametersObject().getAlpha()*dValue - 
							probFromAlphaDValue * (model.getParametersObject().inspectionCost + 
									(model.getParametersObject().getAlpha()*dValue * model.getParametersObject().getFine()));				}

					//System.out.println("D vs C. Payoff is " + payoffWithNeighbor);

			} else if (neighbor.getCurrentStrategy() == ModelParameters.DEFECTOR) {
				
				if (this.currentStrategy == ModelParameters.COOPERATOR) {
					
					// CASE C VS. D  (= reputational reward - inspection cost)
					
					float probFromAlphaDValue;
					
					if (valueEdge.equals("high")) {
						probFromAlphaDValue = model.getParametersObject().getProbToBeInspectedAlphaDH();
					} else {
						probFromAlphaDValue = model.getParametersObject().getProbToBeInspectedAlphaDL();
					}

					payoffWithNeighbor = model.getParametersObject().getReputationalReward() - 
							probFromAlphaDValue * model.getParametersObject().inspectionCost;
					
					//System.out.println("C vs D. Payoff is " + payoffWithNeighbor);

				} else if (this.currentStrategy == ModelParameters.DEFECTOR) {
					
					// CASE D VS. D (= saving alpha - inspection cost - probability to be fined)
					
					float probFrom2AlphaDValue, dValue;
					
					if (valueEdge.equals("high")) {
						probFrom2AlphaDValue = model.getParametersObject().getProbToBeInspected2AlphaDH();
						dValue = model.getParametersObject().getValueForDH();
					} else {
						probFrom2AlphaDValue = model.getParametersObject().getProbToBeInspected2AlphaDL();
						dValue = model.getParametersObject().getValueForDL();
					}
					
					payoffWithNeighbor = model.getParametersObject().getAlpha()*dValue - 
							probFrom2AlphaDValue * (model.getParametersObject().inspectionCost + 
									(model.getParametersObject().getAlpha()*dValue * model.getParametersObject().getFine()));
					
					// System.out.println("D vs D. Payoff is " + payoffWithNeighbor);
					 
				}

			} 
			
			// TODO CUSTOMIZE WAY OF AGGREGATION
			if (ModelParameters.PAYOFF_AGGREGATION.compareTo("MAX") == 0) {
				
				// assign if value is higher than previous saved payoff (MAX value)
				if (payoffWithNeighbor > this.currentPayoff)
					this.currentPayoff = payoffWithNeighbor;
				
			} else {

				// add to the global payoff as we are adding values
				this.currentPayoff += payoffWithNeighbor;
			}

		}
		
		// TODO CUSTOMIZE WAY OF AGGREGATION
		if (ModelParameters.PAYOFF_AGGREGATION.compareTo("AVG") == 0) {
			this.currentPayoff /= neighbors.size();
		}
		
		/*
		 * System.out.println("A-" + this.gamerAgentId + " was " +
		 * this.getCurrentStratey() + ", payoff: " + this.currentPayoff);
		 */

		return this.currentPayoff;
	}

	
	/**
	 * With this function we update the strategy using the voter model strategy
	 * 
	 * This method is as simple as selecting one neighbor at random
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return true if the agent has changed its strategy w.r.t. its previous
	 *         strategy
	 */
	private boolean voterModel(SimState state) {

		Model model = (Model) state;

		ArrayList<Integer> neighbors = (ArrayList<Integer>) model.socialNetwork.getNeighborsOfNode(this.gamerAgentId);

		// first check if the agent has neighbors

		if (neighbors.size() > 0) {

			// this j neighbor is the one to imitate by the focal agent
			int j = model.random.nextInt(neighbors.size());
			GamerAgent neighbor = (GamerAgent) (model.getAgents()).get(neighbors.get(j));

			// change the strategy by the one of the randomly selected neighbor
			this.setCurrentStrategy(neighbor.getPreviousStrategy());

			return true;
		}

		return false;
	}

	/**
	 * With this function we update the strategy using the Fermi distribution
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return true if the agent has changed its strategy w.r.t. its previous
	 *         strategy
	 */
	private boolean fermiRule(SimState state) {

		Model model = (Model) state;

		ArrayList<Integer> neighbors = (ArrayList<Integer>) model.socialNetwork.getNeighborsOfNode(this.gamerAgentId);

		// first check if the agent has neighbors

		if (neighbors.size() > 0) {

			// get one neighbor at random
			int j = model.random.nextInt(neighbors.size());
			GamerAgent neighbor = (GamerAgent) (model.getAgents()).get(neighbors.get(j));

			double neighPayOff = 0.;
			double focalAgentPayOff = 0.;

			neighPayOff = neighbor.getPreviousPayoff();
			focalAgentPayOff = this.getPreviousPayoff();

			// calculate the prob using the Fermi distribution and payoffs' difference

			double beta = 5; // as in Santos' papers for Climate Change

			double prob = 1.0 / (1 + Math.exp(-1 * beta * (neighPayOff - focalAgentPayOff)));

			// Check if the agent adopts the neighbor's strategy

			double r = model.random.nextDouble(true, true);

			if (r < prob) {

				// change the strategy!
				this.setCurrentStrategy(neighbor.getPreviousStrategy());

				return true;
			}
		}

		return false;
	}

	/**
	 * With this function we update the strategy using the Wright-Fisher rule. 
	 * This rule consists of selecting one neighbor (including the focal agent) with a
	 * probability given by its payoff. Then, imitate the strategy of this agent
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return true if the agent has changed its strategy w.r.t. its previous
	 *         strategy
	 */
	private boolean wrightFisherRule (SimState state) {

		Model model = (Model) state;

		ArrayList<Integer> neighbors = (ArrayList<Integer>) model.socialNetwork.getNeighborsOfNode(this.gamerAgentId);

		// first check if the agent has neighbors

		if (neighbors.size() > 0) {

			double acc = 0;
			int selectedNeighbor = -1;

			double listFitness[] = new double[neighbors.size() + 1];

			for (int i = 0; i < neighbors.size(); i++) {
				GamerAgent neighbor = (GamerAgent) (model.getAgents()).get(neighbors.get(i));

				// obtain the payoff of this neighbor and add its 'fitness' to a hash map
				double fitness = neighbor.getPreviousPayoff() - model.minPayOff;

				// add the acc fitness to this neighbor
				acc += fitness;
				listFitness[i] = acc;

				// System.out.println("neighbor " + i + " has payoff of " + fitness + ". acc is
				// " + listFitness[i]);
			}

			// do the same for the focal agent itself (it will be the 'size' index)
			double fitness = this.getPreviousPayoff() - model.minPayOff;
			acc += fitness;
			listFitness[neighbors.size()] = acc;

			// System.out.println("focal agent has payoff of " + fitness + ". acc is " +
			// listFitness[neighbors.size()]);

			// divide all values by acc value
			for (int i = 0; i < listFitness.length; i++) {
				listFitness[i] /= acc;

				// System.out.println(i + " ==== " + listFitness[i]);
			}

			// now, get a random number and choose one among them using the wheel roulette

			double r = model.random.nextDouble(true, true);

			for (int i = 0; i < listFitness.length; i++) {

				// System.out.println("r is " + r + " and next threshold is " + listFitness[i]
				// );

				// check if the random number is below the normalized fitness of the i-th
				// element
				if (r < listFitness[i]) {
					// it is, so this is the agent to imitate

					selectedNeighbor = i;
					// System.out.println(selectedNeighbor + " is selected");

					break;
				}
			}

			if (selectedNeighbor == neighbors.size()) {

				// it means, it is the same, no change
				return false;

			} else {

				// change the strategy!
				this.setCurrentStrategy(((GamerAgent) (model.getAgents()).get(neighbors.get(selectedNeighbor)))
						.getPreviousStrategy());

				// Model.log.log(Level.FINE, " Strategy changed!! A-" + this.gamerAgentId + " to
				// strategy " +
				// this.currentStrategy + " by imitating A-" + neighbor.getGamerAgentId() +
				// "\n");

				return true;
			}
		}

		return false;
	}

	/**
	 * With this function we update the strategy using the proportional imitation
	 * rule
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return true if the agent has changed its strategy w.r.t. its previous
	 *         strategy
	 */
	private boolean proportionalImitation(SimState state) {

		Model model = (Model) state;

		ArrayList<Integer> neighbors = (ArrayList<Integer>) model.socialNetwork.getNeighborsOfNode(this.gamerAgentId);

		// first check if the agent has neighbors

		if (neighbors.size() > 0) {

			int j = model.random.nextInt(neighbors.size());
			GamerAgent neighbor = (GamerAgent) (model.getAgents()).get(neighbors.get(j));

			double neighPayOff = 0.;
			double focalAgentPayOff = 0.;

			neighPayOff = neighbor.getPreviousPayoff();
			focalAgentPayOff = this.getPreviousPayoff();

			if (neighPayOff > model.maxPayOff) {
				neighPayOff = model.maxPayOff;
			}

			// compare their payoffs and strategies in the previous step
			if (neighPayOff > focalAgentPayOff) {

				// the neighbor's strategy was better so we try to shift the strategy with a
				// prob.

				double prob = (neighPayOff - focalAgentPayOff) / (model.maxPayOff - model.minPayOff);

				double r = model.random.nextDouble(true, true);

				// Check if the agent adopts the neighbor's strategy
				if (r < prob) {

					// change the strategy!
					this.setCurrentStrategy(neighbor.getPreviousStrategy());

					return true;
				}
			}
		}

		return false;
	}

	/**
	 * With this function we update the strategy using the unconditional imitation
	 * This rule [Novak92] is simple and the focal agents adopts the strategy of the
	 * neighbor with the highest payoff if it is higher than her own. Thus, it is
	 * deterministic and rational choice to maximize the own fitness of the agent by
	 * imitating the most successful one.
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return true if the agent has changed its strategy w.r.t. its previous
	 *         strategy
	 */
	private boolean unconditionalImitation(SimState state) {

		Model model = (Model) state;

		// get the list of neighbors
		ArrayList<Integer> neighbors = (ArrayList<Integer>) model.socialNetwork.getNeighborsOfNode(this.gamerAgentId);

		// first check if the agent has neighbors
		if (neighbors.size() > 0) {

			double payoffBest = -Double.MAX_VALUE;
			byte strategyNeighBest = -1;

			for (int i = 0; i < neighbors.size(); i++) {

				// get i-th neighbor and update best payoff
				GamerAgent neighbor = (GamerAgent) (model.getAgents()).get(neighbors.get(i));

				if (neighbor.getPreviousPayoff() > payoffBest) {
					payoffBest = neighbor.getPreviousPayoff();
					strategyNeighBest = neighbor.getPreviousStrategy();

				}
			}

			// we compare the best payoff of the neighborhood w.r.t. the own payoff to
			// update or not
			if (payoffBest > this.getPreviousPayoff()) {

				this.setCurrentStrategy(strategyNeighBest);

				return true;
			}
		}

		return false;

	}
	
	/**
	 * Mutate the strategy
	 * @return if it is mutated
	 * @param state
	 */
	public boolean mutateStrategy (SimState state) {

		Model model = (Model) state;
		
		double r = model.random.nextDouble (true, true);
		
		if (r < model.getParametersObject().getMutProb()) {
			
			//System.out.print("\n***** Mutated A-" + this.gamerAgentId + " from " + this.currentStrategy);
			
			// mutate to the new strategy (it can be the current one)
			this.currentStrategy = (byte) (1 + model.random.nextInt(2));			
			
			//System.out.println(" to " + this.currentStrategy);
			
			return true;	
			
		} else 
			
			return false;					
	}
	

	/**
	 * With this function we update the strategy used by the agent. We use the
	 * defined imitative ev. dynamics rule for calculating the change (if there is a
	 * change)
	 * 
	 * @param state - a simulation model object (SimState).
	 * @return true if the agent has changed its strategy w.r.t. its previous
	 *         strategy
	 */
	public boolean updateRuleForStrategy(SimState state) {

		Model model = (Model) state;

		// it does not make sense to do it for the first step

		switch (model.getParametersObject().getUpdateRule()) {

		case ModelParameters.PROPORTIONAL_UPDATE_RULE:
			return this.proportionalImitation(state);

		case ModelParameters.UI_UPDATE_RULE:
			return this.unconditionalImitation(state);

		case ModelParameters.VOTER_UPDATE_RULE:

			// hybrid VM with UI. VM is used with prob q while UI is used with prob 1-q
			// when q is 1, the VM is always used for the agent

			double r = model.random.nextDouble(true, true);

			if (r < model.getParametersObject().getQ_VM()) {
				// System.out.println("Agent-" + this.getGamerAgentId() + ": VM used");
				return this.voterModel(state);
			} else {
				// System.out.println("Agent-" + this.getGamerAgentId() + ": UI used");
				return this.unconditionalImitation(state);
			}

		case ModelParameters.FERMI_UPDATE_RULE:
			return this.fermiRule(state);

		case ModelParameters.WF_UPDATE_RULE:
			return this.wrightFisherRule(state);

		}

		return false;

	}

	// --------------------------- Steppable method --------------------------//

	/**
	 * Step of the simulation.
	 * 
	 * @param state - a simulation model object (SimState).
	 */

	// @Override
	public void step(SimState state) {

		Model model = (Model) state;

		currentStep = (int) model.schedule.getSteps();


		// mutate the strategy or not	
		if (! this.mutateStrategy (state)) {
		
			// if we have one step at least, we save current payoff and strategy as previous
			// values
			if (currentStep > 0) {
				
				int T_rounds = model.getParametersObject().getT_rounds();
	
				// check if T rounds has passed to update its rule too
				if ((currentStep % T_rounds) == 0) {
					this.updateRuleForStrategy(state);
				}
			}
		}

	}

	

}
