package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.graphstream.graph.Edge;
import org.apache.commons.math3.stat.correlation.*;

import sim.engine.*;
import sim.util.*;

import socialnetwork.*;
import util.BriefFormatter;


/**
 * Simulation core, responsible for scheduling agents of the EGT model for TAX FRAUD
 * 
 * @author mchica
 * @date 2020/02/03
 * @place Las Palmas GC
 */
public class Model extends SimState {

	// ########################################################################
	// Variables
	// ########################################################################

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static String CONFIGFILENAME;

	// LOGGING
	public static final Logger log = Logger.getLogger(Model.class.getName());

	// MODEL VARIABLES

	ModelParameters params;

	float minPayOff; // min payoff to be obtained by agents for the update rule
	float maxPayOff; // max payoff to be obtained by agents for the update rule

	Bag agents;

	int k_C_Agents[]; // a counter of the k_C agents during the simulation
	int k_D_Agents[]; // a counter of the k_D agents during the simulation

	int strategyChanges[]; // array with the total number of strategy changes during the simulation

	float globalPayoffs[]; // array with the sum of all the payoffs of the population at each step

	// SOCIAL NETWORK
	GraphStreamer socialNetwork;

	int MC_RUN = -1;

	// --------------------------- Clone method ---------------------------//

	// --------------------------- Get/Set methods ---------------------------//
	//

	public static String getConfigFileName() {
		return CONFIGFILENAME;
	}

	public static void setConfigFileName(String _configFileName) {
		CONFIGFILENAME = _configFileName;
	}

	public GraphStreamer getSocialNetwork() {
		return socialNetwork;
	}

	/**
	 * 
	 * @return the payoffs of all the agents
	 */
	public float[] getGlobalPayoffs() {
		return globalPayoffs;
	}

	/**
	 * @param _globalPayoffs is the array with the payoffs for all the agents
	 */
	public void setGlobalPayoffs(float[] _globalPayoffs) {
		this.globalPayoffs = _globalPayoffs;
	}

	/**
	 * 
	 * @return the bag of agents.
	 */
	public Bag getAgents() {
		return agents;
	}

	/**
	 * 
	 * @param _agents is the bag (array) of agents
	 */
	public void setAgents(Bag _agents) {
		this.agents = _agents;
	}

	/**
	 * Get the global net-wealth of the population at a given step
	 * 
	 * @return the number of k_T agents
	 */
	public float getGlobalPayoffAtStep(int _position) {
		return this.globalPayoffs[_position];
	}

	/**
	 * Get the number of k_C agents at a given step - time
	 * 
	 * @return the number of k_C agents
	 */
	public int getk_C_AgentsAtStep(int _position) {
		return k_C_Agents[_position];
	}

	/**
	 * Get the number of k_D agents for all the period of time
	 * 
	 * @return an ArrayList with the evolution of the k_D agents
	 */
	public int[] getk_D_AgentsArray() {
		return k_D_Agents;
	}


	/**
	 * Get the number of k_C agents for all the period of time
	 * 
	 * @return an ArrayList with the evolution of k_C agents
	 */
	public int[] getk_C_AgentsArray() {
		return k_C_Agents;
	}

	/**
	 * Get the number of k_D agents at a given step - time
	 * 
	 * @return the number of k_D agents
	 */
	public int getk_D_AgentsAtStep(int _position) {
		return k_D_Agents[_position];
	}

	/**
	 * Get the number of changes in the strategy of the agents for all the period of
	 * time
	 * 
	 * @return an ArrayList with the evolution of the strategy changes for all the
	 *         agents
	 */
	public int[] getStrategyChanges_AgentsArray() {
		return strategyChanges;
	}

	/**
	 * Get the parameter object with all the input parameters
	 */
	public ModelParameters getParametersObject() {
		return this.params;
	}

	/**
	 * Set the parameters
	 * 
	 * @param _params the object to be assigned
	 */
	public void setParametersObject(ModelParameters _params) {
		this.params = _params;
	}

	// ########################################################################
	// Constructors
	// ########################################################################

	/**
	 * Initializes a new instance of the simulation class.
	 * 
	 * @param _params an object with all the parameters of the model
	 */
	public Model(ModelParameters _params) {

		super(_params.getSeed());

		try {

			// This block configure the logger with handler and formatter
			long millis = System.currentTimeMillis();
			FileHandler fh = new FileHandler("./logs/" + _params.getOutputFile() + "_" + millis + ".log");
			log.addHandler(fh);
			BriefFormatter formatter = new BriefFormatter();
			fh.setFormatter(formatter);

			log.setLevel(Level.FINE);

			log.log(Level.FINE, "Log file created at " + millis + " (System Time Millis)\n");

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// get parameters
		params = _params;

		// store an IDs array with the agents to be k_C, and k_D at the beginning
		// of the simulation
		// This is done according to the parameters of their k_C, and k_D distribution

		// this.scrambledAgentIds = new ArrayList<Integer> ();

		// Initialization of the type of strategy counts
		k_C_Agents = new int[params.getMaxSteps()];
		k_D_Agents = new int[params.getMaxSteps()];

		strategyChanges = new int[params.getMaxSteps()];

		globalPayoffs = new float[params.getMaxSteps()];

		// social network initialization from file

		socialNetwork = new GraphStreamer(params.nrAgents, params);
		socialNetwork.setGraph(params);
		
		minPayOff = maxPayOff = (float) -1.;

	}

	// --------------------------- SimState methods --------------------------//

	/**
	 * Sets up the simulation. The first method called when the simulation.
	 */
	public void start() {

		super.start();

		this.MC_RUN++;

		if ((params.getk_C() + params.getk_D() ) != params.nrAgents) {
			System.err.println("Error with the k_C, k_D distribution. "
					+ "Check k_C + k_D is equal to the total number of agents \n");
		}

		final int FIRST_SCHEDULE = 0;
		int scheduleCounter = FIRST_SCHEDULE;

		// at random, create an array with the IDs unsorted.
		// In this way we can randomly distribute the agents are going to be k_C,
		// and k_D
		// this is done here to change every MC simulation without changing the SN
		// IMPORTANT! IDs of the SN nodes and agents must be the same to avoid BUGS!!

		// init variables for controlling number of agents and payoffs and other summary arrays
		for (int i = 0; i < params.getMaxSteps(); i++) {
			
			k_C_Agents[i] = k_D_Agents[i] = 0;
			strategyChanges[i] = 0;
			globalPayoffs[i] = (float) 0.;

		}

		// calculate max and min payoff values to normalize update rule
		this.minPayOff = 0;  // TODO
		this.maxPayOff = 0;  // TODO

		// System.out.println("min is " + minPayOff + " and max is " + maxPayOff);

		// Initialization of the agents
		agents = new Bag();

		for (int i = 0; i < params.getNrAgents(); i++) {

			// get a random value from the scrambled IDs data structure and remove it

			/*
			 * int randomPos = this.random.nextInt(this.scrambledAgentIds.size()); int
			 * idAgent = this.scrambledAgentIds.get(randomPos);
			 * this.scrambledAgentIds.remove(randomPos);
			 */

			// System.out.print(idAgent + ";");

			byte strategy = ModelParameters.UNDEFINED_STRATEGY;

			if (i < params.k_C) {
				// we need to add it as k_C strategy
				strategy = ModelParameters.COOPERATOR;

			} else if (i < (params.k_C + params.k_D)) {

				// it is a k_D strategy
				strategy = ModelParameters.DEFECTOR;
			}

			// generate the agent, push in the bag, and schedule
			GamerAgent cl = generateAgent(i, strategy);

			// Add agent to the list and schedule
			agents.add(cl);

			// Add agent to the schedule
			schedule.scheduleRepeating(Schedule.EPOCH, scheduleCounter, cl);

		}

		// shuffle agents in the Bag and later, reassign the id to the position in the
		// bag
		agents.shuffle(this.random);
		
		for (int i = 0; i < agents.size(); i++) 
			((GamerAgent) agents.get(i)).setGamerAgentId(i);
							

		// depending on the prob given as a configuration parameter, 
		// we set which link has high or low value
		
		// add an attribute with the value for each edge of the graph
		for (Edge edgeIt : (this.socialNetwork.getGraph()).getEachEdge()) {
					    
		    if (this.random.nextDouble() < this.params.getProbDHigh())
			    edgeIt.setAttribute("d_value", "high");
		    else
		    	edgeIt.setAttribute("d_value", "low");		    
		}		
				
		// Add anonymous agent to calculate statistics
		setAnonymousAgentApriori(scheduleCounter);
		scheduleCounter++;

		setAnonymousAgentAposteriori(scheduleCounter);

	}

	
	// -------------------------- Auxiliary methods --------------------------//

	/**
	 * Generates the agent with its initial strategy, id, probability to act and its
	 * computed activity array (from the latter probability). This is important as
	 * we can save time not to ask at each step if the agent is active or not
	 * 
	 * @param _nodeId   is the id of the agent
	 * @param _strategy is the type of strategy the agent is going to follow
	 * @return the agent created by the method
	 */
	private GamerAgent generateAgent(int _nodeId, byte _strategy) {

		GamerAgent cl = new GamerAgent(_nodeId, _strategy, this.params.maxSteps, this.params.T_rounds, -1);
		
		return cl;
	}

	/**
	 * This function calculates the global wealth of the population for the current
	 * step and it is called when finishing the steps. It counts the number of k_C,
	 * and k_D of the whole population (without taking into account the SN) and
	 * apply the utility matrix.
	 * 
	 * Also, this function updates the individual payoffs for each agent depending
	 * on its strategy
	 * 
	 * @return the global payoff of this step
	 * 
	 * @deprecated
	 */
	
	private double calculatePayoffWithNeighborsGlobally() {

		return -1.;
	}

	/**
	 * Adds the anonymous agent to schedule (at the beginning of each step), which
	 * calculates the statistics.
	 * 
	 * @param scheduleCounter
	 */
	private void setAnonymousAgentApriori(int scheduleCounter) {

		// Add to the schedule at the end of each step
		schedule.scheduleRepeating(Schedule.EPOCH, scheduleCounter, new Steppable() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -2837885990121299044L;

			public void step(SimState state) {


				// log.log(Level.FINE, "Step " + schedule.getSteps() + "\n");
			}
		});

	}

	/**
	 * Adds the anonymous agent to schedule (at the end of each step), which
	 * calculates the statistics.
	 * 
	 * @param scheduleCounter
	 */
	private void setAnonymousAgentAposteriori(int scheduleCounter) {

		// Add to the schedule at the end of each step
		schedule.scheduleRepeating(Schedule.EPOCH, scheduleCounter, new Steppable() {

			private static final long serialVersionUID = 3078492735754898981L;

			public void step(SimState state) {

				int currentStep = (int) schedule.getSteps();

				// System.out.println("\n****Step " + currentStep);

				k_C_Agents[currentStep] = k_D_Agents[currentStep] = 0;
				strategyChanges[currentStep] = 0;

				for (int i = 0; i < params.nrAgents; i++) {

					if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.COOPERATOR) {
						k_C_Agents[currentStep]++;
					}

					if (((GamerAgent) agents.get(i)).getCurrentStrategy() == ModelParameters.DEFECTOR) {
						k_D_Agents[currentStep]++;
					}

					if (((GamerAgent) agents.get(i)).hasChangedStrategyAtStep(currentStep)) {
						strategyChanges[currentStep]++;
					}

				}

				// update the payoffs

				globalPayoffs[currentStep] = (float) 0.;

				// we decide whether to apply a well-mixed population (no SN to calculate
				// payoffs so
				// they are calculated locally) or locally by the SN structure

				if (params.typeOfNetwork == GraphStreamer.NetworkType.WELL_MIXED_POP) {

					// calculate global payoffs and assign the payoff for all the agents of the pop
					// calculatePayoffWithNeighborsGlobally();
					System.out.println("GLOBAL PAYOFFS CALCULATION IS NOT IMPLEMENTED YET");
					
				} else {

					// locally assign payoff

					for (int i = 0; i < params.nrAgents; i++) {

						globalPayoffs[currentStep] += ((GamerAgent) agents.get(i)).calculatePayoffWithNeighbors(state);

						// update previous payoff and strategies values

						((GamerAgent) agents.get(i)).setPreviousPayoff(((GamerAgent) agents.get(i)).getCurrentPayoff());
						((GamerAgent) agents.get(i))
								.setPreviousStrategy(((GamerAgent) agents.get(i)).getCurrentStrategy());

					}
				}

				// show the result of the last step in the console
				if (currentStep == (params.getMaxSteps() - 1)) {
					
					log.log(Level.FINE,
							"Final step;wealth;" + globalPayoffs[currentStep] + ";k_C;" + k_C_Agents[currentStep]
									+ ";k_D;" + k_D_Agents[currentStep] + ";strategyChanges;" 
									+ strategyChanges[currentStep] + "\n");

				}					
				
				// plotting and saving additional output information for analysis
				plotSaveAdditionalInfo(currentStep);

			}
		});

	}

	/**
	 * This method wraps all the processes of saving and plotting additional
	 * information to study the dynamics of the simulation (e.g., SN, correlation
	 * between agents, or spatial/temporal correlations in the output)
	 * 
	 * Depending if the static variables of Model (SHOW_SN, OUTPUT_LATTICE,
	 * CALCULATE_SPATIO_TEMP_CORR etc...) are activated or not, the function will do
	 * and save all the required information
	 * 
	 * @param _currentStep is the step of the simulation
	 * 
	 */

	protected void plotSaveAdditionalInfo(int _currentStep) {

		
		// ALL EVOLUTION: This information is for saving in a file all the evolution of the sim steps to analyze
		// the evolution as a signal (FFT for instance)
		if ((ModelParameters.OUTPUT_WHOLE_EVOLUTION) && (_currentStep == (params.maxSteps - 1))) {

			try {

				// save
				File fileCorrCoef = new File("./logs/evolution_FFT/" + "WholeEvolution_" + params.getOutputFile() + "."
						+ this.MC_RUN + ".txt");
				PrintWriter printWriter = new PrintWriter(fileCorrCoef);

				printWriter.write("step;netWealth;k_C;k_D;strategyChanges;\n");

				// loop for all the steps
				for (int k = 0; k < (params.maxSteps - 1); k++) {

					// print all info of the step
					printWriter.write(k + ";" + this.globalPayoffs[k] + ";"
							+ this.k_C_Agents[k] + ";" + this.k_D_Agents[k] + ";" + this.strategyChanges[k] + ";\n");

				}

				printWriter.close();

			} catch (FileNotFoundException e) {

				e.printStackTrace();
				log.log(Level.SEVERE, e.toString(), e);
			}

		}
		
		if ((ModelParameters.OUTPUT_STRENGTH_STRATEGIES) && (_currentStep == (params.maxSteps - 1))) {

			try {

				// save
				File fileStrength = new File("./logs/" + "Strength_Strategies_" + params.getOutputFile() + "."
						+ this.MC_RUN + ".txt");
				PrintWriter printWriter = new PrintWriter(fileStrength);

				printWriter.write("agentId;degree;strength;strategy;\n");

				double[] strategies = new double[agents.size()];
				double[] strength = new double[agents.size()];
				
				// print strength of the nodes and its state
				for (int i = 0; i < agents.size(); i++) {
					
					int id = ((GamerAgent)agents.get(i)).getGamerAgentId();

					ArrayList<Integer> neighbors = (ArrayList<Integer>) socialNetwork.getNeighborsOfNode(i);
					
					strength[i] = 0;
					
					for (int j = 0; j < neighbors.size(); j++) {
						
						int idNeighbor = ((GamerAgent) (agents).get(neighbors.get(j))).getGamerAgentId();
						
						// get DH or DL from the edge between the focal agent and its neighbor
						String valueEdge = (String) (socialNetwork.getAttributeEdge
								(id, idNeighbor, "d_value"));
					
						if (valueEdge.equals("high")) {
							strength[i] += params.getValueForDH();
						} else {
							strength[i] += params.getValueForDL();
						}
						
					}
					
					// print into a file all info about the nodes

					if (((GamerAgent)agents.get(i)).getCurrentStrategy() == ModelParameters.COOPERATOR) {
						
						printWriter.write(id + ";" + neighbors.size() + ";" + strength[i] + ";C;\n");					
						strategies[i] = ModelParameters.COOPERATOR;
					
					} else {
						
						printWriter.write(id + ";" + neighbors.size() + ";" + strength[i] + ";D;\n");					
						strategies[i] = ModelParameters.DEFECTOR;
					}
				
				}
				
				// also stores Pearson correlation at the end of the file
				
				printWriter.write("\nPearson's correlation;" + new PearsonsCorrelation().correlation(strength, strategies)
						+ ";Spearman's correlation;" + new SpearmansCorrelation().correlation(strength, strategies) + ";\n");					
				
				printWriter.close();

			} catch (FileNotFoundException e) {

				e.printStackTrace();
				log.log(Level.SEVERE, e.toString(), e);
			}

		}
		
		

		
	}

	// ----------------------------- I/O methods -----------------------------//

	/**
	 * Prints simple statistics evolution during the time.
	 */
	public void printStatisticsScreen() {

		GamerAgent gamerAgent;

		int allk_C, allk_D;
		int tmp;

		allk_C = allk_D = 0;

		for (int i = 0; i < params.nrAgents; i++) {

			gamerAgent = (GamerAgent) agents.get(i);
			
			tmp = gamerAgent.getCurrentStrategy();

			if (tmp == ModelParameters.COOPERATOR)
				allk_C++;

			if (tmp == ModelParameters.DEFECTOR)
				allk_D++;


			// obtain avg. degree of the agent
			ArrayList<Integer> neighbors = (ArrayList<Integer>) this.socialNetwork
					.getNeighborsOfNode(gamerAgent.gamerAgentId);

			System.out.println("A-" + gamerAgent.getGamerAgentId() + ";" + +neighbors.size() + ";" + ";"
					+ gamerAgent.getCurrentStrategy());

		}

		// Print it out
		System.out.println();
		System.out.print("k_C;" + allk_C);
		System.out.print(";k_D;" + allk_D);
		System.out.println();

	}
}
