package util;

import java.io.IOException;

import org.graphstream.algorithm.Toolkit;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSinkDGS;
import org.graphstream.stream.file.FileSourceDGS;
import org.graphstream.stream.file.FileSourceGEXF;
import org.apache.commons.cli.*;   // for CLI parsing arguments

//----------------------------- MAIN FUNCTION -----------------------------//

/**
 * Main function to read SNs from GEXF file (two columns for edges) and store into a DGS file (GraphStream).
 * 
 * @param args
 */

public class ParserGexf2DGS {		
	
	// PRIVATE METHODS
		
		
	/** 
	 *   MAIN FUNCTION
	 */
	public static void main (String[] args) {

		String gexfFileName = "";
		String outputFileName = "";
	    
	    try {
	    	
	    	// parsing the options
			Options options = new Options();
			
			options.addOption("gexfFile", true, "Gexf file with the edges between nodes");
			options.getOption("gexfFile").setRequired(true);
			options.getOption("gexfFile").setOptionalArg(false);

			options.addOption("outputDGSName", true, "DGS file for the converted network");
			options.getOption("outputDGSName").setRequired(true);
			
			
			
			// create the parser
		    CommandLineParser parser = new DefaultParser();
		    
	        // parse the command line arguments for the given options
	        CommandLine line = parser.parse (options, args);
	        
	        // retrieve the arguments
	        		    
		    if( line.hasOption( "gexfFile" ) )		    
		    	gexfFileName = line.getOptionValue("gexfFile");
		    
			if( line.hasOption( "outputDGSName" ) )			    
		    	outputFileName = line.getOptionValue("outputDGSName");		    	  	

		    // help information
		    if( line.hasOption("help") ) {
			    	
			    // automatically generate the help statement
			    HelpFormatter formatter = new HelpFormatter();
			    formatter.printHelp( "ParserSNs2DGS. 4th July 2016. Manuel Chica. UOC", options );			   	
			}

		    FileSourceGEXF fileSource = new FileSourceGEXF();
			Graph graphFromFile = new SingleGraph("SNFromFile");

			fileSource.addSink(graphFromFile);
			fileSource.readAll(gexfFileName);

			fileSource.removeSink(graphFromFile);

			// save to a file
			FileSinkDGS fileSink = new FileSinkDGS();
			fileSink.writeAll(graphFromFile,  outputFileName + ".dgs");
			
			// some stats

			System.out.println("Density: "+ Toolkit.density(graphFromFile));
			System.out.println("Avg. degree: " + Toolkit.averageDegree(graphFromFile));
			System.out.println("Diameter: " + Toolkit.diameter(graphFromFile));
			System.out.println("CC: " + Toolkit.averageClusteringCoefficient(graphFromFile));
			
			System.out.println("No. of nodes: " + graphFromFile.getNodeCount() 
					+ "\nNo. of edges: " + graphFromFile.getEdgeSet().size() );

		
			System.out.println("---------------\nParsing ended successfully" + 
					gexfFileName + " to DGS file " + outputFileName);
			
	    } catch( org.apache.commons.cli.ParseException exp ) {
	    	
	        // oops, something went wrong
	        System.err.println( "Parsing failed. Reason: " + exp.getMessage() );
	        
	    } catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
	    		

	}
	
}
