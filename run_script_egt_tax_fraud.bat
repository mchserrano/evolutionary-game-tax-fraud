

:: TIME EVOLUTION DIFFERENT ALPHAS

:: java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile simple_10k_FERMI_alpha_0_1.txt -alpha 0.1
::java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile simple_10k_FERMI_alpha_0_2 -alpha 0.2
::java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile simple_10k_FERMI_alpha_0_3 -alpha 0.3
::java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile simple_10k_FERMI_alpha_0_4 -alpha 0.4
::java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile simple_10k_FERMI_alpha_0_5 -alpha 0.5
::java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile simple_10k_FERMI_alpha_0_6 -alpha 0.6



:: DIFFERENT INITIAL POP CONDITIONS

:: java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_1 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 500 -alpha 0.1
:: java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_2 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 500 -alpha 0.2
:: java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_3 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000-SA_cooperators_step 500 -alpha 0.3
:: java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_4 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 500 -alpha 0.4
:: java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_5 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 500 -alpha 0.5
:: java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_pop_10k_FERMI_alpha_0_6 -SA -SA_cooperators_min 0 -SA_cooperators_max 10000 -SA_cooperators_step 500 -alpha 0.6

:: DIFFERENT SNs

java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_FERMI.properties -outputFile SA_alpha_10k_FERMI_withMut_ADD -SA_ALPHA
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_SF_m2_FERMI.properties -outputFile SA_alpha_10k_FERMI_withMut_ADD_SF_m2 -SA_ALPHA
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_SF_m3_FERMI.properties -outputFile SA_alpha_10k_FERMI_withMut_ADD_SF_m3 -SA_ALPHA
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_SF_m4_FERMI.properties -outputFile SA_alpha_10k_FERMI_withMut_ADD_SF_m4 -SA_ALPHA
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_SF_m5_FERMI.properties -outputFile SA_alpha_10k_FERMI_withMut_ADD_SF_m5 -SA_ALPHA
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_SF_m6_FERMI.properties -outputFile SA_alpha_10k_FERMI_withMut_ADD_SF_m6 -SA_ALPHA
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_SF_m7_FERMI.properties -outputFile SA_alpha_10k_FERMI_withMut_ADD_SF_m7 -SA_ALPHA
java -jar egt_tax_fraud.jar -paramsFile ./config/10k_agents_SF_real/params_10kagents_SF_m8_FERMI.properties -outputFile SA_alpha_10k_FERMI_withMut_ADD_SF_m8 -SA_ALPHA


